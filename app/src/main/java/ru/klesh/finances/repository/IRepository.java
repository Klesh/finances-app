package ru.klesh.finances.repository;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Collection;

/**
 * Created by klesh on 05.01.17.
 */

public interface IRepository<K, V> {

    @Nullable
    V get(K key);

    @NonNull
    Collection<V> getAll();

    V remove(K key) throws RepositoryException;

    void clear();

    K save(@NonNull V value) throws RepositoryException;
}
