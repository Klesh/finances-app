package ru.klesh.finances.repository;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by klesh on 05.01.17.
 */

public abstract class PagedRepository<K, V> extends CacheableRepository<Integer, Map<K, V>> implements IRepository<K, V> {

    protected int pageCount = 0;
    protected int pageSize = 5;
    protected Map<Integer, Map<K, V>> cache;

    protected PagedRepository(String cacheFileName, int pageSize) {
        super(cacheFileName);
        this.pageSize = pageSize;
        readCacheFromFile(cache);
    }

    protected abstract K onSave(V value) throws RepositoryException;

    protected abstract V onRemove(V value) throws RepositoryException;

    protected abstract void onPageRequest(Map<K, V> cache, int pageSize, int pageIndex) throws RuntimeException;

    public Collection<V> getNextPage() {
        Map<K, V> newPage = new HashMap<>();
        onPageRequest(newPage, pageSize, ++pageCount);
        cache.put(pageCount, newPage);
        writeCacheToFile(cache);
        return newPage.values();
    }

    @Override
    public K save(@NonNull V value) throws RepositoryException {
        K key = onSave(value);
        Map<K, V> page = cache.get(pageCount);

        if (page.size() >= pageSize) {
            page = new HashMap<>();
            pageCount++;
            cache.put(pageCount, page);
        }

        page.put(key, value);
        writeCacheToFile(cache);

        return key;
    }


    @Nullable
    @Override
    public V get(K key) {
        for (Map<K, V> map : cache.values()) {
            V v = map.get(key);
            if (v != null) {
                return v;
            }
        }

        return null;
    }

    @Override
    public void clear() {
        cache.clear();
        writeCacheToFile(cache);
    }

    @Override
    public V remove(K key) throws RepositoryException {
        V v = null;

        for (Map<K, V> map : cache.values()) {
            v = map.remove(key);
            if (v != null) {
                break;
            }
        }

        if (v != null) {
            v = onRemove(v);
            writeCacheToFile(cache);
        }
        return v;
    }

    @NonNull
    @Override
    public Collection<V> getAll() {
        List<V> all = new ArrayList<>();

        for (Map<K, V> map : cache.values()) {
            all.addAll(map.values());
        }

        return all;
    }
}
