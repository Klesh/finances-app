package ru.klesh.finances.repository.filtering;

import java.util.ArrayList;
import java.util.Collection;

import ru.klesh.finances.api.entity.Entity;
import ru.klesh.finances.api.entity.IFilterableEntity;

public class EntityFilter<V extends IFilterableEntity & Entity> {

    public Collection<V> search(String search, Collection<V> all) {
        if (search == null) {
            return all;
        }

        search = search.toLowerCase();
        Collection<V> founded = new ArrayList<>();

        for (V v : all) {
            if (v.canSearch(search)) {
                founded.add(v);
            }
        }

        return founded;
    }
}