package ru.klesh.finances.repository;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Map;

import ru.klesh.finances.FinancesApp;
import ru.klesh.finances.utils.IOUtils;

/**
 * Created by klesh on 05.01.17.
 */

public abstract class CacheableRepository<K, V> {

    private File cacheFile;
    protected String cacheFileName;

    protected CacheableRepository(@NonNull String cacheFileName) {
        cacheFile = new File(FinancesApp.externalFilesDir, cacheFileName);
        this.cacheFileName = cacheFileName;
        Log.i(getClass().getName(), "Init Cacheable repository with cache file: " + cacheFile.getAbsolutePath());

    }

    protected void readCacheFromFile(@NonNull Map<K, V> cache) {
        if (cacheFile.exists()) {
            try {
                String data = IOUtils.readAllLines(cacheFile);
                cache.putAll(new Gson().<Map<K, V>>fromJson(data, getCacheType()));
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }

    protected void writeCacheToFile(@NonNull Map<K, V> cache) {
        if (!recreateCacheFile()) {
            Log.v(getClass().getName(), "Can't write cache to file: " + cacheFile.getAbsolutePath());
            return;
        }

        try {
            IOUtils.writeToFile(new Gson().toJson(cache), cacheFile);
        } catch (Throwable e) {
            e.printStackTrace();
        }

    }

    private boolean recreateCacheFile() {
        if (cacheFile.exists()) {
            if (!cacheFile.delete()) {
                Log.v(getClass().getName(), "Can't delete cache file: " + cacheFile.getAbsolutePath());
                return false;
            }
        }

        try {
            return cacheFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    protected abstract Type getCacheType();
}
