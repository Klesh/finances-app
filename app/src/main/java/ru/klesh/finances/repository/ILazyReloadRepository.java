package ru.klesh.finances.repository;

/**
 * Created by Klesh on 20.01.2017.
 */
public interface ILazyReloadRepository<K, V> extends IRepository<K, V>, IReloadableRepository<K, V> {
    long getUpToDatePeriod();

    void setUpToDatePeriod(long upToDatePeriodSec);

    void reloadImmediately() throws RepositoryException;

    void makeUpToDate();

    boolean isUpToDate();
}
