package ru.klesh.finances.repository.impl;

import android.util.Log;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import ru.klesh.finances.api.entity.IdEntity;
import ru.klesh.finances.api.entity.ResponseEntity;
import ru.klesh.finances.api.impl.FinancesApi;
import ru.klesh.finances.api.impl.entity.CategoriesEntity;
import ru.klesh.finances.api.impl.entity.CategoriesEntity.CategoryData;
import ru.klesh.finances.api.impl.entity.PaymentsEntity;
import ru.klesh.finances.repository.ReloadableRepository;
import ru.klesh.finances.repository.RepositoryException;
import ru.klesh.finances.repository.filtering.EntityFilter;
import ru.klesh.finances.repository.IDataFilter;

/**
 * Created by klesh on 04.01.17.
 */

public class CategoriesRepository extends ReloadableRepository<Integer, CategoryData> implements IDataFilter<CategoryData> {

    private PaymentsRepository paymentsRepository = new PaymentsRepository();

    private final String TAG = "CategoriesRepository";

    private EntityFilter<CategoryData> entityFilter = new EntityFilter<>();

    public CategoriesRepository() {
        super("repo.categories");
    }

    @Override
    protected void onReload(Map<Integer, CategoryData> cache) throws RepositoryException {

        CategoriesEntity categoriesEntity = FinancesApi.getInstance().getCategories();

        if (categoriesEntity.hasError()) {
            throw new RepositoryException(categoriesEntity.getError());
        }

        for (CategoryData data : categoriesEntity.getResponseData()) {
            cache.put(data.getId(), data);
        }
    }

    @Override
    protected void onPostReload(Map<Integer, CategoryData> cache) throws RepositoryException {
        super.onPostReload(cache);

        paymentsRepository.reload();
        for (PaymentsEntity.PaymentData payData : paymentsRepository.getAll()) {
            CategoryData cat = cache.get(payData.getCategoryId());
            if (cat != null) {
                cat.setTotalMoney(cat.getTotalMoney() + payData.getValue());
            } else {
                Log.w(TAG, "Skip computing total money value, can't find category for: " + payData.toString());
            }
        }
    }

    @Override
    protected Integer onSave(CategoryData value) throws RepositoryException {
        if (value.isNew()) {
            IdEntity idEntity = FinancesApi.getInstance().addCategory(value);

            if (idEntity.hasError()) {
                throw new RepositoryException(idEntity.getError());
            }

            value.setId(idEntity.getResponseData().getId());
            return value.getId();
        } else {
            ResponseEntity r = FinancesApi.getInstance().editCategory(value);

            if (r.hasError()) {
                throw new RepositoryException(r.getError());
            }

            return value.getId();
        }
    }

    @Override
    protected CategoryData onRemove(CategoryData value) throws RepositoryException {
        IdEntity.IdData idData = new IdEntity.IdData();
        idData.setId(value.getId());

        ResponseEntity responseEntity = FinancesApi.getInstance().removeCategory(idData);

        if (responseEntity.hasError()) {
            throw new RepositoryException(responseEntity.getError());
        }

        return value;
    }

    public List<String> getAllCategoryNames() {
        List<String> strings = new ArrayList<>();

        for (CategoryData categoryData : getAll()) {
            strings.add(categoryData.getName());
        }

        return strings;
    }

    @Override
    protected Type getCacheType() {
        return new TypeToken<Map<Integer, CategoryData>>() {
        }.getType();
    }

    @Override
    public Collection<CategoryData> search(String search) {
        return entityFilter.search(search, getAll());
    }
}
