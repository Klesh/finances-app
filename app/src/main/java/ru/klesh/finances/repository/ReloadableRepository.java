package ru.klesh.finances.repository;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by klesh on 09.01.17.
 */

public abstract class ReloadableRepository<K, V> extends LazyReloadRepository<K, V> {

    protected ReloadableRepository(String cacheFileName) {
        super(cacheFileName);
    }

    protected void onPreReload(Map<K, V> cache) throws RepositoryException {
    }

    protected abstract void onReload(Map<K, V> cache) throws RepositoryException;

    protected void onPostReload(Map<K, V> cache) throws RepositoryException {
    }

    @Override
    public void reload() throws RepositoryException {
        if (!isUpToDate()) {
            return;
        }

        onPreReload(cache);
        Map<K, V> newCache = new HashMap<>();
        onReload(newCache);
        cache.clear();
        cache.putAll(newCache);
        writeCacheToFile(cache);
        makeUpToDate();
        onPostReload(cache);
    }
}
