package ru.klesh.finances.repository;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by klesh on 09.01.17.
 */

public abstract class ReloadablePagedRepository<K, V> extends PagedRepository<K, V> implements IReloadableRepository<K, V>  {

    protected ReloadablePagedRepository(String cacheFileName, int pageSize) {
        super(cacheFileName, pageSize);
    }

    protected void onPreReload() throws RepositoryException {
    }

    protected void onPostReload() throws RepositoryException {
    }

    @Override
    public void reload() throws RepositoryException {
        onPreReload();

        Map<Integer, Map<K, V>> newCache = new HashMap<>();
        Map<K, V> page = new HashMap<>();

        for (int i = 0; i < pageCount; i++) {
            page.clear();
            onPageRequest(page, pageSize, i);
            newCache.put(i, page);
        }

        cache.clear();
        cache.putAll(newCache);
        writeCacheToFile(cache);

        onPostReload();
    }
}
