package ru.klesh.finances.repository.impl;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Map;

import ru.klesh.finances.api.entity.IdEntity;
import ru.klesh.finances.api.entity.ResponseEntity;
import ru.klesh.finances.api.impl.FinancesApi;
import ru.klesh.finances.api.impl.entity.PaymentsEntity;
import ru.klesh.finances.api.impl.entity.PaymentsEntity.PaymentData;
import ru.klesh.finances.repository.IDataAggregator;
import ru.klesh.finances.repository.IDataFilter;
import ru.klesh.finances.repository.ReloadableRepository;
import ru.klesh.finances.repository.RepositoryException;
import ru.klesh.finances.repository.filtering.EntityAggregator;
import ru.klesh.finances.repository.filtering.EntityFilter;

/**
 * Created by klesh on 04.01.17.
 */

public class PaymentsRepository extends ReloadableRepository<Integer, PaymentData> implements IDataFilter<PaymentData>, IDataAggregator<PaymentData> {

    private EntityAggregator<PaymentData> aggregatorByDate = new EntityAggregator<>();
    private EntityFilter<PaymentData> entityFilter = new EntityFilter<>();

    public PaymentsRepository() {
        super("repo.payments");
    }

    @Override
    protected void onReload(Map<Integer, PaymentData> cache) throws RepositoryException {
        PaymentsEntity paymentsEntity = FinancesApi.getInstance().getPayments();

        if (paymentsEntity.hasError()) {
            throw new RepositoryException(paymentsEntity.getError());
        }

        for (PaymentData data : paymentsEntity.getResponseData()) {
            cache.put(data.getId(), data);
        }
    }

    @Override
    protected Integer onSave(PaymentData value) throws RepositoryException {
        if (value.isNew()) {
            IdEntity idEntity = FinancesApi.getInstance().addPayment(value);

            if (idEntity.hasError()) {
                throw new RepositoryException(idEntity.getError());
            }

            value.setId(idEntity.getResponseData().getId());
            return value.getId();
        } else {
            ResponseEntity r = FinancesApi.getInstance().editPayment(value);

            if (r.hasError()) {
                throw new RepositoryException(r.getError());
            }

            return value.getId();
        }
    }

    @Override
    protected PaymentData onRemove(PaymentData value) throws RepositoryException {
        IdEntity.IdData idData = new IdEntity.IdData();
        idData.setId(value.getId());

        ResponseEntity responseEntity = FinancesApi.getInstance().removePayment(idData);

        if (responseEntity.hasError()) {
            throw new RepositoryException(responseEntity.getError());
        }

        return value;
    }

    @Override
    protected Type getCacheType() {
        return new TypeToken<Map<Integer, PaymentData>>() {
        }.getType();
    }

    @Override
    public Collection<PaymentData> search(String search) {
        return entityFilter.search(search, getAll());
    }

    @Override
    public Collection<Collection<PaymentData>> aggregate(String type) {
        return aggregatorByDate.aggregate(type, getAll());
    }


}
