package ru.klesh.finances.repository;

/**
 * Created by klesh on 05.01.17.
 */

public interface IReloadableRepository<K, V> extends IRepository<K, V> {
    void reload() throws RepositoryException;
}
