package ru.klesh.finances.repository.filtering;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import ru.klesh.finances.api.entity.Entity;
import ru.klesh.finances.api.entity.IAggregableEntity;

public class EntityAggregator<V extends IAggregableEntity<V> & Entity> {

    public Collection<Collection<V>> aggregate(String type, Collection<V> all) {
        Collection<V> visited = new ArrayList<>();
        Collection<Collection<V>> groups = new ArrayList<>();


        for (V data : all) {
            if (visited.contains(data)) {
                continue;
            }

            List<V> group = new ArrayList<>();
            for (V sub : all) {
                if (visited.contains(data)) {
                    continue;
                }

                if (sub.canAggregate(type, data)) {
                    group.add(sub);
                    visited.add(sub);
                }
            }

            groups.add(group);
        }

        return groups;
    }
}