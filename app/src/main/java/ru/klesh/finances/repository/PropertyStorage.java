package ru.klesh.finances.repository;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by klesh on 09.01.17.
 */

public class PropertyStorage extends CacheableRepository<String, Object> {

    protected HashMap<String, Object> cache = new HashMap<>();

    protected PropertyStorage(@NonNull String cacheFileName) {
        super(cacheFileName);
        readCacheFromFile(cache);
    }

    public Object get(String key) {
        return cache.get(key);
    }

    @Nullable
    public String getString(String key) {
        return getString(key, null);
    }

    public int getInt(String key) {
        return getInt(key, 0);
    }

    public long getLong(String key) {
        return getLong(key, 0L);
    }

    public double getDouble(String key) {
        return getDouble(key, 0D);
    }

    public float getFloat(String key) {
        return getFloat(key, 0F);
    }

    public boolean getBoolean(String key) {
        return getBoolean(key, false);
    }

    public String getString(String key, String def) {
        Object o = get(key);
        return o != null ? (String) o : def;
    }

    public int getInt(String key, int def) {
        Object o = get(key);
        return o != null ? (int) o : def;
    }

    public long getLong(String key, long def) {
        Object o = get(key);
        if (o instanceof Double) {
            return (long) (double) o;
        }
        return o != null ? (long) o : def;
    }

    public double getDouble(String key, double def) {
        Object o = get(key);
        return o != null ? (double) o : def;
    }

    public float getFloat(String key, float def) {
        Object o = get(key);
        return o != null ? (float) o : def;
    }

    public boolean getBoolean(String key, boolean def) {
        Object o = get(key);
        return o != null ? (boolean) o : def;
    }

    public void set(String key, Object value) {
        cache.put(key, value);
    }

    public void unset(String key) {
        cache.remove(key);
    }

    public void save() {
        writeCacheToFile(cache);
    }

    @Override
    protected Type getCacheType() {
        return new TypeToken<Map<String, Object>>() {
        }.getType();
    }
}
