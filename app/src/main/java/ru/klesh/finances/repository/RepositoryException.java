package ru.klesh.finances.repository;

import java.io.IOException;

/**
 * Created by klesh on 05.01.17.
 */

public class RepositoryException extends Exception {
    public RepositoryException() {
    }

    public RepositoryException(String detailMessage) {
        super(detailMessage);
    }

    public RepositoryException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public RepositoryException(Throwable throwable) {
        super(throwable);
    }
}
