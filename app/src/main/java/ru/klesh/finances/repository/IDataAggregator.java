package ru.klesh.finances.repository;

import java.util.Collection;

import ru.klesh.finances.api.entity.Entity;
import ru.klesh.finances.api.entity.IAggregableEntity;

/**
 * Created by Klesh on 23.01.2017.
 */

public interface IDataAggregator<V extends IAggregableEntity<V> & Entity> {
    Collection<Collection<V>> aggregate(String type);
}
