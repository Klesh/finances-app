package ru.klesh.finances.repository;

/**
 * Created by Klesh on 20.01.2017.
 */

public abstract class LazyReloadRepository<K, V> extends Repository<K, V> implements ILazyReloadRepository<K, V> {
    private long lastReloadTime = -1;
    private long upToDatePeriod = 1000 * 5L;

    protected LazyReloadRepository(String cacheFileName) {
        super(cacheFileName);
    }

    @Override
    public long getUpToDatePeriod() {
        return upToDatePeriod;
    }

    @Override
    public void setUpToDatePeriod(long upToDatePeriodSec) {
        this.upToDatePeriod = upToDatePeriodSec;
    }

    @Override
    public void reloadImmediately() throws RepositoryException {
        lastReloadTime = 0L;
        reload();
    }

    @Override
    public void makeUpToDate() {
        lastReloadTime = System.currentTimeMillis();
    }

    @Override
    public boolean isUpToDate() {
        return System.currentTimeMillis() - lastReloadTime >= upToDatePeriod;
    }
}
