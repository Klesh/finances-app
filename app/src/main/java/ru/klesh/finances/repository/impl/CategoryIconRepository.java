package ru.klesh.finances.repository.impl;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Map;

import ru.klesh.finances.api.entity.RepositoryEntity;
import ru.klesh.finances.repository.SimpleLocalStorage;

/**
 * Created by Klesh on 23.07.2017.
 */

public class CategoryIconRepository extends SimpleLocalStorage<Integer, CategoryIconRepository.CategoryIconData> {

    public CategoryIconRepository() {
        super("category.icon.repo");
    }

    @Override
    protected Type getCacheType() {
        return new TypeToken<Map<Integer, CategoryIconData>>() {
        }.getType();
    }

    public static class CategoryIconData extends RepositoryEntity<Integer> {
        private String icon;

        public CategoryIconData() {
        }

        public CategoryIconData(int categoryId, String icon) {
            this.id = categoryId;
            this.icon = icon;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }
    }
}
