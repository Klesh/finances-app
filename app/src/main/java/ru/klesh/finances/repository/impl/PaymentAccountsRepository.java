package ru.klesh.finances.repository.impl;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Map;

import ru.klesh.finances.api.entity.IdEntity;
import ru.klesh.finances.api.entity.ResponseEntity;
import ru.klesh.finances.api.impl.FinancesApi;
import ru.klesh.finances.api.impl.entity.PaymentAccountsEntity;
import ru.klesh.finances.api.impl.entity.PaymentAccountsEntity.PaymentAccountData;
import ru.klesh.finances.repository.IDataFilter;
import ru.klesh.finances.repository.ReloadableRepository;
import ru.klesh.finances.repository.RepositoryException;
import ru.klesh.finances.repository.filtering.EntityFilter;

/**
 * Created by klesh on 04.01.17.
 */

public class PaymentAccountsRepository extends ReloadableRepository<Integer, PaymentAccountData> implements IDataFilter<PaymentAccountData> {

    private EntityFilter<PaymentAccountData> entityFilter = new EntityFilter<>();

    public PaymentAccountsRepository() {
        super("repo.accounts");
    }

    @Override
    protected void onReload(Map<Integer, PaymentAccountData> cache) throws RepositoryException {
        PaymentAccountsEntity accounts = FinancesApi.getInstance().getPaymentAccounts();

        if (accounts.hasError()) {
            throw new RepositoryException(accounts.getError());
        }

        for (PaymentAccountData data : accounts.getResponseData()) {
            cache.put(data.getId(), data);
        }
    }

    @Override
    protected Integer onSave(PaymentAccountData value) throws RepositoryException {
        if (value.isNew()) {
            IdEntity idEntity = FinancesApi.getInstance().addPaymentAccount(value);

            if (idEntity.hasError()) {
                throw new RepositoryException(idEntity.getError());
            }

            value.setId(idEntity.getResponseData().getId());
            return value.getId();
        } else {
            ResponseEntity r = FinancesApi.getInstance().editPaymentAccount(value);

            if (r.hasError()) {
                throw new RepositoryException(r.getError());
            }

            return value.getId();
        }
    }

    public int indexById(int id) {
        int index = 0;

        for (PaymentAccountData accountData : getAll()) {
            if (accountData.getId().equals(id)) {
                return index;
            }
            index++;
        }

        return -1;
    }

    @Override
    protected PaymentAccountData onRemove(PaymentAccountData value) throws RepositoryException {
        IdEntity.IdData idData = new IdEntity.IdData();
        idData.setId(value.getId());

        ResponseEntity responseEntity = FinancesApi.getInstance().removePaymentAccount(idData);

        if (responseEntity.hasError()) {
            throw new RepositoryException(responseEntity.getError());
        }

        return value;
    }

    @Override
    protected Type getCacheType() {
        return new TypeToken<Map<Integer, PaymentAccountData>>() {
        }.getType();
    }

    @Override
    public Collection<PaymentAccountData> search(String search) {
        return entityFilter.search(search, getAll());
    }
}
