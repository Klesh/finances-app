package ru.klesh.finances.repository;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import ru.klesh.finances.lib.events.EventBus;

/**
 * Created by klesh on 05.01.17.
 */

public abstract class Repository<K, V> extends CacheableRepository<K, V> implements IRepository<K, V> {

    protected Map<K, V> cache = null;
    protected static int globalRevision = 0;
    protected int localRevision = globalRevision;

    protected Repository(String cacheFileName) {
        super(cacheFileName);
        initCache();
    }

    @Override
    protected void finalize() throws Throwable {
        EventBus.INSTANCE.unsubscribeAll(this);
        super.finalize();
    }

    private void initCache() {
        cache = new HashMap<>();
        readCacheFromFile(cache);
    }

    protected abstract K onSave(V value) throws RepositoryException;

    protected abstract V onRemove(V value) throws RepositoryException;

    @Nullable
    @Override
    public V get(K key) {
        if (globalRevision != localRevision) {
            initCache();
            localRevision = globalRevision;
        }

        return cache.get(key);
    }

    @NonNull
    @Override
    public Collection<V> getAll() {
        return cache.values();
    }

    @Override
    public K save(@NonNull V value) throws RepositoryException {
        K key = onSave(value);
        cache.put(key, value);
        writeCacheToFile(cache);
        globalRevision++;
        localRevision++;
        return key;
    }

    @Override
    public V remove(K key) throws RepositoryException {
        V v = cache.remove(key);
        if (v != null) {
            v = onRemove(v);
            writeCacheToFile(cache);
            globalRevision++;
            localRevision++;
        }
        return v;
    }

    @Override
    public void clear() {
        cache.clear();
        writeCacheToFile(cache);
        globalRevision++;
        localRevision++;
    }
}
