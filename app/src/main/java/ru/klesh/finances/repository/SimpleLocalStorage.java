package ru.klesh.finances.repository;

import ru.klesh.finances.api.entity.RepositoryEntity;

/**
 * Created by Klesh on 23.07.2017.
 */

public abstract class SimpleLocalStorage<K, V extends RepositoryEntity<K>> extends Repository<K, V> {

    protected SimpleLocalStorage(String cacheFileName) {
        super(cacheFileName);
    }

    @Override
    protected K onSave(V value) throws RepositoryException {
        return value.getId();
    }

    @Override
    protected V onRemove(V value) throws RepositoryException {
        return value;
    }
}
