package ru.klesh.finances.repository;

import java.util.Collection;

import ru.klesh.finances.api.entity.IFilterableEntity;

/**
 * Created by Klesh on 21.01.2017.
 */

public interface IDataFilter<V extends IFilterableEntity> {
    Collection<V> search(String search);
}
