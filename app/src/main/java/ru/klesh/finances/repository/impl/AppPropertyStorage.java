package ru.klesh.finances.repository.impl;

import ru.klesh.finances.repository.PropertyStorage;

/**
 * Created by klesh on 10.01.17.
 */

public class AppPropertyStorage extends PropertyStorage {

    public static final AppPropertyStorage INSTANCE = new AppPropertyStorage();

    private AppPropertyStorage() {
        super("app.properties");
    }
}
