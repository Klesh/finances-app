package ru.klesh.finances.utils.http;

/**
 * Created by Klesh on 21.07.2017.
 */

public class DateHelper {

    public static long removeTimePart(long datetime) {
        return (datetime / 86400) * 86400;
    }

    public static long removeDatePart(long datetime) {
        return datetime % 86400;
    }
}
