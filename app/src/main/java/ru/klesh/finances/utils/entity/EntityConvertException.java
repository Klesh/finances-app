package ru.klesh.finances.utils.entity;

public class EntityConvertException extends Exception {
        public EntityConvertException(String detailMessage) {
            super(detailMessage);
        }

        public EntityConvertException(String detailMessage, Throwable throwable) {
            super(detailMessage, throwable);
        }

        public EntityConvertException(Throwable throwable) {
            super(throwable);
        }
    }