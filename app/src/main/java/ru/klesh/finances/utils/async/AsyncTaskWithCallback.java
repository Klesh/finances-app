package ru.klesh.finances.utils.async;

import android.os.AsyncTask;

/**
 * Created by klesh on 09.01.17.
 */

public abstract class AsyncTaskWithCallback<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {

    private Callback<Result> callback = null;

    public AsyncTaskWithCallback() {
    }

    public AsyncTaskWithCallback(Callback<Result> callback) {
        this.callback = callback;
    }

    public Callback<Result> getCallback() {
        return callback;
    }

    public void setCallback(Callback<Result> callback) {
        if (callback != null) {
            this.callback = callback;
        }
    }

    protected void callback(Result result) {
        if (callback != null) {
            callback.onCall(result);
        }
    }

    protected void callbackError(Throwable t) {
        if (callback != null) {
            callback.onError(t);
        }
    }
}
