package ru.klesh.finances.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Klesh on 17.01.2017.
 */

public class DateFormatter {
    public static final String DEFAULT_DATE_FORMAT = "dd.MM.yyyy";
    public static final String DEFAULT_TIME_FORMAT = "HH:mm:ss";
    public static final String DEFAULT_DATE_TIME_FORMAT = DEFAULT_DATE_FORMAT + " " + DEFAULT_TIME_FORMAT;

    private static final DateFormatter INSTANCE = new DateFormatter();

    private SimpleDateFormat dateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT, Locale.US);
    private Date cachedDate = null;

    public static long nowTimestamp() {
        return System.currentTimeMillis() / 1000;
    }

    public static String now() {
        return now(DEFAULT_DATE_FORMAT);
    }

    public static String now(String format) {
        return format(nowTimestamp(), format);
    }

    public static String format(long timestamp) {
        return format(timestamp, DEFAULT_DATE_FORMAT);
    }

    public static String format(long timestamp, String format) {
        timestamp = timestamp * 1000;

        Date cachedDate = INSTANCE.cachedDate;

        if (cachedDate != null && cachedDate.getTime() == timestamp) {
            return format(cachedDate, format);
        }

        return format(new Date(timestamp), format);
    }

    public static String format(Date date) {
        return format(date, DEFAULT_DATE_FORMAT);
    }

    public static String format(Date date, String format) {
        INSTANCE.cachedDate = date;
        SimpleDateFormat dateFormat = INSTANCE.dateFormat;
        dateFormat.applyPattern(format);
        return dateFormat.format(date);
    }

    public static long parse(String data) {
        return parse(data, DEFAULT_DATE_FORMAT);
    }

    public static long parse(String data, String format) {
        SimpleDateFormat dateFormat = INSTANCE.dateFormat;
        dateFormat.applyPattern(format);
        try {
            return dateFormat.parse(data).getTime() / 1000;
        } catch (ParseException e) {
            return -1L;
        }
    }
}
