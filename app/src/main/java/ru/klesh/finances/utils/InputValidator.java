package ru.klesh.finances.utils;

/**
 * Created by klesh on 04.01.17.
 */

public class InputValidator {
    public static boolean inNumber(String intStr) {
        try {
            Long.parseLong(intStr);
        } catch (NumberFormatException ignored) {
            return false;
        }

        return true;
    }
}
