package ru.klesh.finances.utils.http;

/**
 * Created by klesh on 04.01.17.
 */

public class HttpException extends Exception {
    public HttpException(String detailMessage) {
        super(detailMessage);
    }

    public HttpException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public HttpException(Throwable throwable) {
        super(throwable);
    }
}
