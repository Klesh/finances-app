package ru.klesh.finances.utils.async;

/**
 * Created by klesh on 09.01.17.
 */

public abstract class Callback<Result> {

    public abstract void onCall(Result result);

    public void onError(Throwable result) {

    }
}
