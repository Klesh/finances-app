package ru.klesh.finances.utils;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by klesh on 05.01.17.
 */

public class MessageHelper {

    public static void toastMessage(Context context, int rID, Object... data) {
        toastMessage(context, context.getString(rID), data);
    }

    public static void toastMessage(Context context, int rID, int length, Object... data) {
        toastMessage(context, context.getString(rID), length, data);
    }

    public static void toastMessage(Context context, String template, Object... data) {
        toastMessage(context, template, Toast.LENGTH_LONG, data);
    }

    public static void toastMessage(Context context, String template, int length, Object... data) {
        Toast.makeText(context, StringFormater.format(template, data), length).show();
    }
}
