package ru.klesh.finances.utils.compact;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Build;

/**
 * Created by klesh on 11.01.17.
 */

public class CompactUtils {

    public static int getColorFromResources(Context context, int resId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return context.getResources().getColor(resId, null);
        } else {
            return context.getResources().getColor(resId);
        }
    }

    public static ColorStateList getColorStateListFromResources(Context context, int resId) {
        return ColorStateList.valueOf(getColorFromResources(context, resId));
    }
}
