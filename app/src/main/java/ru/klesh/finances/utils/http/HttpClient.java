package ru.klesh.finances.utils.http;

import android.support.annotation.NonNull;
import android.util.Log;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import ru.klesh.finances.api.entity.Entity;
import ru.klesh.finances.utils.entity.EntityConverter;

/**
 * Created by klesh on 04.01.17.
 */

public class HttpClient {

    private static HttpClient INSTANCE = new HttpClient();

    private final String TAG_HTTP_CLIENT = getClass().getName();

    private EntityConverter entityConverter = new EntityConverter();

    @NonNull
    public static <T extends Entity> T get(String url, Class<T> responseEntityClass) throws HttpException {
        try {
            String data = get(url);
            return INSTANCE.entityConverter.convertToEntity(data, responseEntityClass);
        } catch (Throwable t) {
            throw new HttpException(t);
        }
    }

    public static <T extends Entity> void post(String url, Entity... requestEntities) throws HttpException {
        try {
            post(url, INSTANCE.entityConverter.convertToSend(requestEntities));
        } catch (Throwable t) {
            throw new HttpException(t);
        }
    }

    @NonNull
    public static <T extends Entity> T post(String url, Class<T> responseEntityClass, Entity... requestEntities) throws HttpException {
        try {
            String data = post(url, INSTANCE.entityConverter.convertToSend(requestEntities));
            return INSTANCE.entityConverter.convertToEntity(data, responseEntityClass);
        } catch (Throwable t) {
            throw new HttpException(t);
        }
    }

    @NonNull
    public static String get(String url) throws HttpException {
        try {
            return INSTANCE.executeGet(url);
        } catch (Throwable t) {
            throw new HttpException(t);
        }
    }

    @NonNull
    public static String post(String url, String body) throws HttpException {
        try {
            return INSTANCE.executePost(url, body);
        } catch (Throwable t) {
            throw new HttpException(t);
        }
    }

    @NonNull
    private String executeGet(String targetURL) throws IOException {
        HttpURLConnection connection = null;

        Log.i(TAG_HTTP_CLIENT, ">>> HTTP GET: " + targetURL);

        try {
            connection = createConnection(targetURL, "GET");
            String s = readAllFromConnection(connection);

            Log.i(TAG_HTTP_CLIENT, "<<< HTTP GET: " + s);

            return s;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    @NonNull
    private String executePost(String targetURL, String body) throws IOException {
        HttpURLConnection connection = null;

        Log.i(TAG_HTTP_CLIENT, ">>> HTTP POST: " + targetURL + " -> " + body);

        try {
            connection = createConnection(targetURL, "POST");
            byte[] outputBytes = body.getBytes("UTF-8");
            connection.setRequestProperty("Content-Length", Integer.toString(outputBytes.length));
            writeAllToConnection(outputBytes, connection);
            String s = readAllFromConnection(connection);

            Log.i(TAG_HTTP_CLIENT, "<<< HTTP POST: " + s);

            return s;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    @NonNull
    private HttpURLConnection createConnection(String targetURL, String method) throws IOException {
        URL url = new URL(targetURL);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod(method);
        connection.setRequestProperty("Content-Type",
                "application/x-www-form-urlencoded");
        connection.setRequestProperty("Content-Language", "en-US");

        connection.setUseCaches(false);
        connection.setDoOutput(true);
        return connection;
    }

    @NonNull
    private String readAllFromConnection(HttpURLConnection connection) throws IOException {
        InputStream is = connection.getInputStream();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        StringBuilder response = new StringBuilder(); // or StringBuffer if Java version 5+
        String line;
        while ((line = rd.readLine()) != null) {
            response.append(line);
            response.append("\n");
        }
        rd.close();
        return response.toString();
    }

    private void writeAllToConnection(byte[] bytes, HttpURLConnection connection) throws IOException {
        DataOutputStream wr = new DataOutputStream(
                connection.getOutputStream());
        wr.write(bytes);
        wr.close();
    }
}
