package ru.klesh.finances.utils;

import android.content.Context;

/**
 * Created by klesh on 05.01.17.
 */

public class StringFormater {


    public static String format(Context context, int rID, Object... data) {
        return format(context.getString(rID), data);
    }

    public static String format(String template, Object... data) {
        return String.format(template, data);
    }

    public static String join(String separator, Object... data) {
        StringBuilder sb = new StringBuilder();

        for (Object o : data) {
            sb.append(String.valueOf(o)).append(separator);
        }

        int spLen = separator.length();
        int sbLen = sb.length();

        if (sbLen > spLen) {
            return sb.substring(0, sbLen - spLen);
        }

        return sb.toString();
    }
}
