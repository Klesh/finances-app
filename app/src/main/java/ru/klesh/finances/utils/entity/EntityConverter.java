package ru.klesh.finances.utils.entity;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ru.klesh.finances.api.entity.Entity;

/**
 * Created by klesh on 04.01.17.
 */

public class EntityConverter {

    private final String TAG_ENTITY_CONVERTER = "EntityConverter";
    private Gson gson = new Gson();

    @NonNull
    public String convertToSend(Entity entity) throws EntityConvertException {
        try {
            return gson.toJson(entity);
        } catch (Throwable t) {
            throw new EntityConvertException(t);
        }
    }

    @NonNull
    public String convertToSend(Entity... entities) throws EntityConvertException {
        try {
            List<JsonObject> elements = new ArrayList<>();

            for (Entity e : entities) {
                if (e == null) {
                    continue;
                }

                JsonElement jsonElement = gson.toJsonTree(e);

                if (jsonElement.isJsonObject()) {
                    elements.add(jsonElement.getAsJsonObject());
                } else {
                    Log.w(TAG_ENTITY_CONVERTER, "Skip non json object entity: " + jsonElement.toString());
                }
            }

            JsonObject bundle = new JsonObject();

            for (JsonObject el : elements) {
                for (Map.Entry<String, JsonElement> entry : el.entrySet()) {
                    bundle.add(entry.getKey(), entry.getValue());
                }
            }

            return bundle.toString();
        } catch (Throwable t) {
            throw new EntityConvertException(t);
        }
    }

    @NonNull
    public <T extends Entity> T convertToEntity(String entityData, Class<T> entityClass) throws EntityConvertException {
        try {
            return gson.fromJson(entityData, entityClass);
        } catch (Throwable t) {
            throw new EntityConvertException(t);
        }
    }
}
