package ru.klesh.finances;

import android.app.Application;

import java.io.File;

public class FinancesApp extends Application {

    public static final String APP_PROP_PHONE = "phone";
    public static final String APP_PROP_PASSWORD = "pass";
    public static final String APP_PROP_BALANCE = "balance";
    public static final String APP_PROP_RICHEST = "richest";

    public static File externalFilesDir = null;

    //// TODO: 16.01.2017 Remove when currencies will be obtaining from server side (Payment accounts feature)
    public static final char CURRENCY_SYMBOL = '\u20BD';

    @Override
    public void onCreate() {
        externalFilesDir = getExternalFilesDir(null);
        super.onCreate();
    }
}