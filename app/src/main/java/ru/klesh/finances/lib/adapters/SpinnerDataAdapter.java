package ru.klesh.finances.lib.adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Klesh on 17.01.2017.
 */

public abstract class SpinnerDataAdapter<T> extends ArrayAdapter<String> {

    private List<T> data;

    public SpinnerDataAdapter(Context context, Collection<T> data) {
        super(context, android.R.layout.simple_spinner_item);
        setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.data = new ArrayList<>(data);

        for (T item : data) {
            add(getText(item));
        }
    }

    public List<T> getData() {
        return data;
    }

    protected abstract String getText(T item);

    @Nullable
    public T getItemData(int position) {
        return data.get(position);
    }
}
