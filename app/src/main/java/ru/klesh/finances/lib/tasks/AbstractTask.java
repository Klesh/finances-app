package ru.klesh.finances.lib.tasks;

import android.content.Context;

import com.afollestad.materialdialogs.MaterialDialog;

import ru.klesh.finances.R;
import ru.klesh.finances.utils.async.AsyncTaskWithCallback;

public abstract class AbstractTask<Result> extends AsyncTaskWithCallback<Void, Void, Result> {

    private MaterialDialog mProgressDialog;
    protected Context mContext;
    private int mResIdLoadingText;
    protected Throwable mError;

    public AbstractTask(Context context, int resIdLoadingText) {
        mContext = context;
        mResIdLoadingText = resIdLoadingText;
    }

    @Override
    protected void onPreExecute() {
        mProgressDialog = new MaterialDialog.Builder(mContext)
                .title(mResIdLoadingText)
                .content(R.string.please_wait)
                .progress(true, 0)
                .show();
    }

    @Override
    protected void onPostExecute(final Result success) {
        mProgressDialog.hide();
        callback(success);

        if (mError != null) {
            callbackError(mError);
        }
    }

    @Override
    protected void onCancelled() {
        mProgressDialog.hide();
    }
}