package ru.klesh.finances.lib.dialogs;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;

import org.apmem.tools.layouts.FlowLayout;

import ru.klesh.finances.R;
import ru.klesh.finances.lib.EnumCategoryIcon;
import ru.klesh.finances.utils.async.Callback;

/**
 * Created by Klesh on 12.01.2017.
 */

public class CategoryIconDialog extends ExtraResultDialog implements View.OnClickListener {

    public static final String KEY_CATEGORY_ICON_DATA = "categoryIcon";
    public static final String KEY_ACTION_TYPE = "actType";

    public static final int ACTION_TYPE_SAVE = 0;

    private static LayoutInflater inflater = null;

    private FlowLayout mFlowLayout;

    public CategoryIconDialog(@NonNull Context context, Callback<Result> callback) {
        super(context, callback);
        title(R.string.select_icon);
        customView(R.layout.dialog_select_category_icon, true);
        negativeText(R.string.cancel);
        show();

        mFlowLayout = (FlowLayout) customView.findViewById(R.id.select_category_icon_layout);

        assert mFlowLayout != null;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        for (EnumCategoryIcon icon : EnumCategoryIcon.values()) {
            View v = inflater.inflate(R.layout.select_category_icon_item, null);
            ImageButton btn = (ImageButton) v.findViewById(R.id.select_category_icon_item_btn);
            btn.setImageResource(icon.getResId());
            btn.setTag(icon);
            btn.setOnClickListener(this);

            mFlowLayout.addView(btn);
        }
    }

    @Override
    public void onClick(View view) {
        setResult(KEY_ACTION_TYPE, ACTION_TYPE_SAVE);
        setResult(KEY_CATEGORY_ICON_DATA, view.getTag());
        instance.hide();
        finishDialog();
    }
}
