package ru.klesh.finances.lib.dialogs;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import ru.klesh.finances.R;
import ru.klesh.finances.api.impl.entity.CategoriesEntity.CategoryData;
import ru.klesh.finances.api.impl.entity.PaymentAccountsEntity.PaymentAccountData;
import ru.klesh.finances.api.impl.entity.PaymentsEntity.PaymentData;
import ru.klesh.finances.lib.adapters.SpinnerAccountsAdapter;
import ru.klesh.finances.lib.adapters.SpinnerCategoriesAdapter;
import ru.klesh.finances.lib.tasks.AddEditPaymentTask;
import ru.klesh.finances.repository.impl.CategoriesRepository;
import ru.klesh.finances.repository.impl.PaymentAccountsRepository;
import ru.klesh.finances.utils.DateFormatter;
import ru.klesh.finances.utils.MessageHelper;
import ru.klesh.finances.utils.async.Callback;

/**
 * Created by Klesh on 12.01.2017.
 */

public class PaymentDialog extends ExtraResultDialog implements MaterialDialog.SingleButtonCallback {

    public static final String KEY_PAYMENT_DATA = "payment";
    public static final String KEY_ACTION_TYPE = "actType";

    public static final int ACTION_TYPE_EDIT = 0;
    public static final int ACTION_TYPE_ADD = 1;
    public static final int ACTION_TYPE_REMOVE = 2;

    private Spinner mAccounts;
    private TextView mDate;
    private TextView mMoney;
    private TextView mComment;
    private Spinner mCategories;

    private PaymentData mPaymentData;

    private CategoriesRepository categoriesRepository = new CategoriesRepository();
    private PaymentAccountsRepository accountsRepository = new PaymentAccountsRepository();

    private SpinnerCategoriesAdapter mCategoriesAdapter;
    private SpinnerAccountsAdapter mAccountsAdapter;

    public PaymentDialog(@NonNull Context context, Callback<ExtraResultDialog.Result> callback) {
        this(context, callback, null);
    }

    public PaymentDialog(@NonNull Context context, Callback<ExtraResultDialog.Result> callback, PaymentData presetData) {
        super(context, callback);
        title(presetData == null ? R.string.add_payment : R.string.edit_payment);
        customView(R.layout.dialog_add_payment, true);
        positiveText(presetData == null ? R.string.add_text : R.string.save);
        negativeText(R.string.cancel);
        autoDismiss(false);
        onAny(this);
        show();

        mPaymentData = presetData;

        mDate = (TextView) customView.findViewById(R.id.dialog_add_payment_date);
        mMoney = (TextView) customView.findViewById(R.id.dialog_add_payment_money);
        mComment = (TextView) customView.findViewById(R.id.dialog_add_payment_comment);
        mAccounts = (Spinner) customView.findViewById(R.id.dialog_add_payment_account_spinner);
        mCategories = (Spinner) customView.findViewById(R.id.dialog_add_payment_category_spinner);

        assert mDate != null;
        assert mMoney != null;
        assert mComment != null;
        assert mAccounts != null;
        assert mCategories != null;

        mDate.setText(DateFormatter.now());
        mCategoriesAdapter = new SpinnerCategoriesAdapter(context, categoriesRepository.getAll());
        mCategories.setAdapter(mCategoriesAdapter);

        mAccountsAdapter = new SpinnerAccountsAdapter(context, accountsRepository.getAll());
        mAccounts.setAdapter(mAccountsAdapter);

        if (mPaymentData != null) {
            mDate.setText(DateFormatter.format(mPaymentData.getTime()));
            mMoney.setText(String.valueOf(mPaymentData.getValue()));
            mComment.setText(mPaymentData.getComment());

            int selectedIndex = categoriesRepository.getAllCategoryNames().indexOf(mPaymentData.getCategory());
            if (selectedIndex >= 0) {
                mCategories.setSelection(selectedIndex);
            }

            selectedIndex = accountsRepository.indexById(mPaymentData.getAccountId());
            if (selectedIndex >= 0) {
                mAccounts.setSelection(selectedIndex);
            }
        }
    }

    @Override
    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
        switch (which) {
            case POSITIVE:
                addSaveClick();
                break;
            default:
                instance.hide();
        }
    }

    private void addSaveClick() {
        setResult(KEY_ACTION_TYPE, ACTION_TYPE_EDIT);

        if (mPaymentData == null) {
            setResult(KEY_ACTION_TYPE, ACTION_TYPE_ADD);
            mPaymentData = new PaymentData();
        }

        long date = DateFormatter.parse(mDate.getText().toString());
        if (date >= 0) {
            mPaymentData.setTime(date);
        } else {
            mDate.setError(context.getString(R.string.enter_valid_date));
            mDate.requestFocus();
            return;
        }

        if (mMoney.getText().length() == 0) {
            mMoney.setError(context.getString(R.string.enter_valid_value));
            mMoney.requestFocus();
            return;
        }

        try {
            mPaymentData.setValue(Integer.valueOf(mMoney.getText().toString()));
        } catch (NumberFormatException e) {
            mMoney.setError(context.getString(R.string.enter_valud_number));
            mMoney.requestFocus();
            return;
        }

        CategoryData selectedCategory = mCategoriesAdapter.getItemData(mCategories.getSelectedItemPosition());
        if (selectedCategory == null) {
            MessageHelper.toastMessage(context, R.string.select_valid_value);
            mCategories.requestFocus();
            return;
        }

        PaymentAccountData selectedAccount = mAccountsAdapter.getItemData(mAccounts.getSelectedItemPosition());
        if (selectedAccount == null) {
            MessageHelper.toastMessage(context, R.string.select_valid_value);
            mAccounts.requestFocus();
            return;
        }

        mPaymentData.setCategoryId(selectedCategory.getId());
        mPaymentData.setCategory(selectedCategory.getName());
        mPaymentData.setAccountId(selectedAccount.getId());
        mPaymentData.setComment(mComment.getText().toString());

        AddEditPaymentTask task = new AddEditPaymentTask(context, mPaymentData);
        task.setCallback(new Callback<PaymentData>() {
            @Override
            public void onCall(PaymentData paymentData) {
                setResult(KEY_PAYMENT_DATA, paymentData);
                instance.hide();
                finishDialog();
            }

            @Override
            public void onError(Throwable result) {
                super.onError(result);
                MessageHelper.toastMessage(context, R.string.template_cant_process_payment, result.toString());
            }
        });
        task.execute();
    }
}
