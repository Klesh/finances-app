package ru.klesh.finances.lib.tasks;

import android.content.Context;

import ru.klesh.finances.R;
import ru.klesh.finances.api.impl.entity.CategoriesEntity.CategoryData;
import ru.klesh.finances.repository.impl.CategoriesRepository;

public class RemoveCategoryTask extends AbstractTask<CategoryData> {

    private int mCategoryId;
    private CategoriesRepository categoriesRepository = new CategoriesRepository();

    public RemoveCategoryTask(Context context, int id) {
        super(context, R.string.remove_category);
        mCategoryId = id;
    }

    @Override
    protected CategoryData doInBackground(Void... params) {
        try {
            return categoriesRepository.remove(mCategoryId);
        } catch (Exception e) {
            mError = e;
            return null;
        }
    }
}