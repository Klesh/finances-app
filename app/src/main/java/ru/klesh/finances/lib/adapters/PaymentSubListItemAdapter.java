package ru.klesh.finances.lib.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;

import java.util.Collection;

import ru.klesh.finances.FinancesApp;
import ru.klesh.finances.R;
import ru.klesh.finances.api.impl.entity.PaymentsEntity.PaymentData;
import ru.klesh.finances.lib.EnumCategoryIcon;
import ru.klesh.finances.lib.dialogs.ExtraResultDialog;
import ru.klesh.finances.lib.dialogs.PaymentDialog;
import ru.klesh.finances.lib.tasks.RemovePaymentTask;
import ru.klesh.finances.repository.impl.CategoryIconRepository;
import ru.klesh.finances.repository.impl.CategoryIconRepository.CategoryIconData;
import ru.klesh.finances.utils.MessageHelper;
import ru.klesh.finances.utils.async.Callback;
import ru.klesh.finances.utils.compact.CompactUtils;

public class PaymentSubListItemAdapter extends HolderItemAdapter<PaymentData, PaymentSubListItemAdapter.ViewHolder> {

    private int colorRowOdd;

    private static CategoryIconRepository categoryIconRepository = new CategoryIconRepository();

    public PaymentSubListItemAdapter(Context context, Collection<PaymentData> data) {
        super(context, R.layout.list_sub_item_payment);
        colorRowOdd = CompactUtils.getColorFromResources(context, R.color.colorRowOdd);
        setData(data);
    }

    @Override
    protected ViewHolder createHolder(View vi) {
        ViewHolder h = new ViewHolder();
        h.category = (TextView) vi.findViewById(R.id.payment_list_sub_item_category);
        h.money = (TextView) vi.findViewById(R.id.payment_list_sub_item_money);
        h.comment = (TextView) vi.findViewById(R.id.payment_list_sub_item_comment);
        h.editBtn = (ImageButton) vi.findViewById(R.id.list_sub_item_payment_edit_btn);
        h.deleteBtn = (ImageButton) vi.findViewById(R.id.list_sub_item_payment_delete_btn);
        h.categoryIcon = (ImageView) vi.findViewById(R.id.payment_list_sub_item_category_image);
        h.swipeLayout = (SwipeLayout) vi.findViewById(R.id.list_sub_item_payment_swipe_layout);
        h.background = vi.findViewById(R.id.list_item_background);

        assert h.category != null;
        assert h.money != null;
        assert h.comment != null;
        assert h.background != null;
        assert h.editBtn != null;
        assert h.deleteBtn != null;
        assert h.categoryIcon != null;

        return h;
    }

    @Override
    protected void actualizeHolder(int position, final ViewHolder h, ViewGroup parent) {
        final PaymentData data = this.getData().get(position);

        h.category.setText(data.getCategory());
        String text = String.valueOf(data.getValue()) + FinancesApp.CURRENCY_SYMBOL;
        h.money.setText(text);
        h.comment.setText(data.getComment());

        if (data.getValue() >= 0) {
            h.money.setTextColor(CompactUtils.getColorStateListFromResources(context, R.color.colorIncomeMoney));
        } else {
            h.money.setTextColor(CompactUtils.getColorStateListFromResources(context, R.color.colorOutcomeMoney));
        }

        CategoryIconData iconData = categoryIconRepository.get(data.getCategoryId());
        if (iconData != null) {
            EnumCategoryIcon icon = EnumCategoryIcon.safeValueOf(iconData.getIcon());
            h.categoryIcon.setImageResource(icon.getResId());
        }

        h.background.setBackgroundColor(position % 2 == 0 ? colorRowOdd : 0);
        h.swipeLayout.close();

        h.editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                h.swipeLayout.close();
                editClick(data);
            }
        });

        h.deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                h.swipeLayout.close();
                removeClick(data);
            }
        });

        h.swipeLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                h.swipeLayout.toggle();
                return false;
            }
        });

    }

    private void editClick(PaymentData paymentData) {
        new PaymentDialog(context, new Callback<ExtraResultDialog.Result>() {
            @Override
            public void onCall(ExtraResultDialog.Result result) {
                PaymentData paymentData = result.get(PaymentDialog.KEY_PAYMENT_DATA);
                int actionType = result.get(PaymentDialog.KEY_ACTION_TYPE);

                if (paymentData == null || actionType < 0) {
                    return;
                }

                if (actionType == PaymentDialog.ACTION_TYPE_ADD) {
                    getData().add(paymentData);
                } else if (actionType == PaymentDialog.ACTION_TYPE_REMOVE) {
                    getData().remove(paymentData);
                }

                notifyDataSetChanged();
            }
        }, paymentData);
    }

    private void removeClick(PaymentData paymentData) {
        if (paymentData != null) {
            RemovePaymentTask task = new RemovePaymentTask(context, paymentData.getId());
            task.setCallback(new Callback<PaymentData>() {
                public void onCall(PaymentData result) {
                    notifyDataItemRemoved(result);
                }

                @Override
                public void onError(Throwable result) {
                    super.onError(result);
                    MessageHelper.toastMessage(context, R.string.template_cant_remove_payment, result.toString());
                }
            });
            task.execute();
        }
    }

    static class ViewHolder {
        TextView category;
        TextView money;
        TextView comment;
        ImageButton editBtn;
        ImageButton deleteBtn;
        ImageView categoryIcon;
        SwipeLayout swipeLayout;
        View background;
    }
}