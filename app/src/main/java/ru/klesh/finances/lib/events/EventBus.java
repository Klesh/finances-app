package ru.klesh.finances.lib.events;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Klesh on 23.07.2017.
 */

public class EventBus {
    public static final EventBus INSTANCE = new EventBus();

    private LinkedHashMap<Class, List<BroadcastEventHandler>> handlerMap = new LinkedHashMap<>();

    public void subscribe(BroadcastEventHandler handler) {
        List<BroadcastEventHandler> handlers = handlerMap.get(handler.getEventClass());
        if (handlers == null) {
            handlers = new ArrayList<>();
            handlerMap.put(handler.getEventClass(), handlers);
        }

        handlers.add(handler);
    }

    public boolean unsubscribe(BroadcastEventHandler handler) {
        List<BroadcastEventHandler> handlers = handlerMap.get(handler.getEventClass());
        if (handlers == null) {
            handlers = new ArrayList<>();
        }

        Iterator<BroadcastEventHandler> it = handlers.iterator();
        while (it.hasNext()) {
            BroadcastEventHandler h = it.next();
            if (h == handler) {
                it.remove();
                return true;
            }
        }

        return false;
    }

    public boolean unsubscribeAll(Object owner) {
        boolean b = false;

        for (List<BroadcastEventHandler> handlers : handlerMap.values()) {
            Iterator<BroadcastEventHandler> it = handlers.iterator();
            while (it.hasNext()) {
                BroadcastEventHandler h = it.next();
                if (h.getOwner() == owner) {
                    it.remove();
                    b = true;
                }
            }
        }

        return b;
    }

    public void post(IBroadcastEvent event) {
        List<BroadcastEventHandler> handlers = handlerMap.get(event.getClass());
        if (handlers != null) {
            for (BroadcastEventHandler h : handlers) {
                if (h.getEventClass().equals(event.getClass())) {
                    h.onEvent(event);
                }
            }
        }
    }
}
