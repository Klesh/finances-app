package ru.klesh.finances.lib.dialogs;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ru.klesh.finances.R;
import ru.klesh.finances.api.EnumCurrency;
import ru.klesh.finances.api.impl.entity.PaymentAccountsEntity.PaymentAccountData;
import ru.klesh.finances.lib.adapters.SpinnerCurrenciesAdapter;
import ru.klesh.finances.lib.tasks.AddEditAccountTask;
import ru.klesh.finances.utils.MessageHelper;
import ru.klesh.finances.utils.async.Callback;

/**
 * Created by Klesh on 12.01.2017.
 */

public class AccountDialog extends ExtraResultDialog implements MaterialDialog.SingleButtonCallback {

    public static final String KEY_PAYMENT_ACCOUNT_DATA = "account";
    public static final String KEY_ACTION_TYPE = "actType";

    public static final int ACTION_TYPE_EDIT = 0;
    public static final int ACTION_TYPE_ADD = 1;
    public static final int ACTION_TYPE_REMOVE = 2;

    private TextView mName;
    private Spinner mCurrencies;

    private PaymentAccountData mPaymentAccountData;

    private SpinnerCurrenciesAdapter mCurrencyAdapter;

    public AccountDialog(@NonNull Context context, Callback<Result> callback) {
        this(context, callback, null);
    }

    public AccountDialog(@NonNull Context context, Callback<Result> callback, PaymentAccountData presetData) {
        super(context, callback);
        title(presetData == null ? R.string.add_account : R.string.edit_account);
        customView(R.layout.dialog_add_account, true);
        positiveText(presetData == null ? R.string.add_text : R.string.save);
        neutralText(R.string.cancel);
        autoDismiss(false);
        onAny(this);
        show();

        mPaymentAccountData = presetData;

        mName = (TextView) customView.findViewById(R.id.dialog_add_payment_account_name);
        mCurrencies = (Spinner) customView.findViewById(R.id.dialog_add_payment_account_currency_spinner);

        assert mName != null;
        assert mCurrencies != null;


        List<EnumCurrency> currencies = new ArrayList<>(Arrays.asList(EnumCurrency.values()));
        currencies.remove(EnumCurrency.UNKNOWN);

        mCurrencyAdapter = new SpinnerCurrenciesAdapter(context, currencies);
        mCurrencies.setAdapter(mCurrencyAdapter);

        if (mPaymentAccountData != null) {
            mName.setText(mPaymentAccountData.getName());

            int index = currencies.indexOf(mPaymentAccountData.getCurrency());
            if (index >= 0) {
                mCurrencies.setSelection(index);
            }
        } else {
            mCurrencies.setSelection(Math.min(0, currencies.size() - 1));
        }
    }

    @Override
    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
        switch (which) {
            case POSITIVE:
                addSaveClick();
                break;
            default:
                instance.hide();
        }
    }

    private void addSaveClick() {
        setResult(KEY_ACTION_TYPE, ACTION_TYPE_EDIT);

        if (mPaymentAccountData == null) {
            setResult(KEY_ACTION_TYPE, ACTION_TYPE_ADD);
            mPaymentAccountData = new PaymentAccountData();
        }

        if (mName.getText().length() == 0) {
            mName.setError(context.getString(R.string.enter_valid_value));
            mName.requestFocus();
            return;
        }

        try {
            mPaymentAccountData.setName(mName.getText().toString());
        } catch (NumberFormatException e) {
            mName.setError(context.getString(R.string.enter_valud_number));
            mName.requestFocus();
            return;
        }

        EnumCurrency currency = mCurrencyAdapter.getItemData(mCurrencies.getSelectedItemPosition());
        if (currency == null) {
            MessageHelper.toastMessage(context, R.string.select_valid_value);
            mCurrencies.requestFocus();
            return;
        }

        mPaymentAccountData.setCurrencyId(currency.getId());

        AddEditAccountTask task = new AddEditAccountTask(context, mPaymentAccountData);
        task.setCallback(new Callback<PaymentAccountData>() {
            @Override
            public void onCall(PaymentAccountData paymentData) {
                setResult(KEY_PAYMENT_ACCOUNT_DATA, paymentData);
                instance.hide();
                finishDialog();
            }

            @Override
            public void onError(Throwable result) {
                super.onError(result);
                MessageHelper.toastMessage(context, R.string.template_cant_process_account, result.toString());
            }
        });
        task.execute();
    }
}
