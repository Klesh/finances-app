package ru.klesh.finances.lib.events;

import ru.klesh.finances.repository.Repository;

/**
 * Created by Klesh on 23.07.2017.
 */

public class RepoChangeEvent implements IBroadcastEvent {
    public String repoPath;
    public Repository repoCaller;

    public RepoChangeEvent(String repoPath, Repository repoCaller) {
        this.repoPath = repoPath;
        this.repoCaller = repoCaller;
    }
}
