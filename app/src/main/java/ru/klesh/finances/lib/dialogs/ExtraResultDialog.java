package ru.klesh.finances.lib.dialogs;

import android.content.Context;

import java.util.HashMap;

import ru.klesh.finances.utils.async.Callback;

/**
 * Created by Klesh on 17.01.2017.
 */

public abstract class ExtraResultDialog extends CallbackDialog<ExtraResultDialog.Result> {

    private Result result;
    private boolean allowCallCallback;

    public ExtraResultDialog(Context context, Callback<Result> callback) {
        super(context, callback);
    }

    protected void setResult(String key, Object object) {
        if (result == null) {
            result = new Result();
        }
        result.set(key, object);
    }

    @Override
    protected void callCallback(Result result) {
        if (!allowCallCallback) {
            throw new RuntimeException("This method can not be called directly!");
        }
        super.callCallback(result);
    }

    protected void finishDialog() {
        allowCallCallback = true;
        callCallback(result);
        allowCallCallback = false;
    }

    public static class Result {
        private HashMap<String, Object> map = new HashMap<>();

        public <T> T get(String key) {
            Object o = map.get(key);
            return (T) o;
        }

        private <T> void set(String key, T object) {
            map.put(key, object);
        }
    }
}
