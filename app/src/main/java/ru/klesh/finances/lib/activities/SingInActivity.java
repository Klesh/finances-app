package ru.klesh.finances.lib.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.pinball83.maskededittext.MaskedEditText;

import ru.klesh.finances.FinancesApp;
import ru.klesh.finances.R;
import ru.klesh.finances.api.impl.FinancesApi;
import ru.klesh.finances.api.impl.entity.ServerSessionEntity;
import ru.klesh.finances.repository.impl.AppPropertyStorage;
import ru.klesh.finances.utils.InputValidator;
import ru.klesh.finances.utils.MessageHelper;

/**
 * A login screen that offers login via email/password.
 */
public class SingInActivity extends AppCompatActivity {

    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;
    private static final long LOGO_WAIT_TIME = 2000;

    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;

    // UI references.
    private MaskedEditText mPhoneView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;

    private AppPropertyStorage propertyStorage = AppPropertyStorage.INSTANCE;
    private Button mSignInButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_singin);
        // Set up the login form.
        mPhoneView = (MaskedEditText) findViewById(R.id.phone);
        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.phone || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        mSignInButton = (Button) findViewById(R.id.sign_in_button);

        assert mSignInButton != null;
        assert mPhoneView != null;
        assert mPasswordView != null;


        mSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        String phone = propertyStorage.getString(FinancesApp.APP_PROP_PHONE);
        String pass = propertyStorage.getString(FinancesApp.APP_PROP_PASSWORD);
        if (phone != null && pass != null) {
            mPhoneView.setEnabled(false);
            mPasswordView.setEnabled(false);
            mSignInButton.setEnabled(false);
            doLogin(phone, pass);
        }
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mPhoneView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String phone = mPhoneView.getUnmaskedText();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid phone address.
        if (TextUtils.isEmpty(phone)) {
            mPhoneView.setError(getString(R.string.error_field_required));
            focusView = mPhoneView;
            cancel = true;
        } else if (!isPhoneValid(phone)) {
            mPhoneView.setError(getString(R.string.error_invalid_phone));
            focusView = mPhoneView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            doLogin(phone, password);
        }
    }

    private boolean isPhoneValid(String phone) {
        return InputValidator.inNumber(phone) && phone.length() == 10;
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

    private void doLogin(String phone, String password) {
        showProgress(true);
        mAuthTask = new UserLoginTask(phone, password);
        mAuthTask.execute((Void) null);
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    public void missingAccountClick(View view) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(FinancesApi.SITE_AUTHOR_VK));
        startActivity(browserIntent);
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mPhone;
        private final String mPassword;
        private ServerSessionEntity mSessionEntity;

        UserLoginTask(String phone, String password) {
            mPhone = phone;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            mSessionEntity = FinancesApi.getInstance(getBaseContext()).singIn(mPhone, mPassword);
            return !mSessionEntity.hasError();
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;

            if (success) {
                setResult(RESULT_OK);

                propertyStorage.set(FinancesApp.APP_PROP_PHONE, mPhone);
                propertyStorage.set(FinancesApp.APP_PROP_PASSWORD, mPassword);
                propertyStorage.save();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getBaseContext(), mSessionEntity.getMessage(), Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }, LOGO_WAIT_TIME);
            } else {
                showProgress(false);
                setResult(RESULT_CANCELED);

                propertyStorage.unset(FinancesApp.APP_PROP_PHONE);
                propertyStorage.unset(FinancesApp.APP_PROP_PASSWORD);
                propertyStorage.save();

                mPhoneView.setEnabled(true);
                mPasswordView.setEnabled(true);
                mSignInButton.setEnabled(true);

                MessageHelper.toastMessage(getBaseContext(), R.string.template_cant_sing_in, mSessionEntity.getError());
                mPasswordView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }
}

