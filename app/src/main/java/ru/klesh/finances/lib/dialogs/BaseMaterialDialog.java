package ru.klesh.finances.lib.dialogs;

import android.content.Context;
import android.support.annotation.NonNull;

import com.afollestad.materialdialogs.MaterialDialog;

/**
 * Created by Klesh on 23.07.2017.
 */

public class BaseMaterialDialog extends MaterialDialog.Builder {

    protected MaterialDialog instance;

    public BaseMaterialDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    public MaterialDialog show() {
        instance = super.show();
        return instance;
    }
}
