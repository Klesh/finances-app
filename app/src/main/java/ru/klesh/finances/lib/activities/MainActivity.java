package ru.klesh.finances.lib.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ru.klesh.finances.FinancesApp;
import ru.klesh.finances.R;
import ru.klesh.finances.api.impl.AppUser;
import ru.klesh.finances.api.impl.FinancesApi;
import ru.klesh.finances.api.impl.entity.BalanceEntity;
import ru.klesh.finances.api.impl.entity.CategoriesEntity.CategoryData;
import ru.klesh.finances.api.impl.entity.PaymentAccountsEntity.PaymentAccountData;
import ru.klesh.finances.api.impl.entity.PaymentsEntity.PaymentData;
import ru.klesh.finances.api.impl.entity.RichestEntity;
import ru.klesh.finances.lib.adapters.AccountsListItemAdapter;
import ru.klesh.finances.lib.adapters.CategoriesListItemAdapter;
import ru.klesh.finances.lib.adapters.PaymentListItemAdapter;
import ru.klesh.finances.lib.adapters.PaymentListItemAdapter.PaymentDataGroup;
import ru.klesh.finances.lib.dialogs.AccountDialog;
import ru.klesh.finances.lib.dialogs.CategoryDialog;
import ru.klesh.finances.lib.dialogs.PaymentDialog;
import ru.klesh.finances.lib.tasks.AddEditAccountTask;
import ru.klesh.finances.lib.tasks.AddEditCategoryTask;
import ru.klesh.finances.lib.tasks.AddEditPaymentTask;
import ru.klesh.finances.lib.widgets.ViewSwitcher;
import ru.klesh.finances.repository.impl.AppPropertyStorage;
import ru.klesh.finances.repository.impl.CategoriesRepository;
import ru.klesh.finances.repository.impl.PaymentAccountsRepository;
import ru.klesh.finances.repository.impl.PaymentsRepository;
import ru.klesh.finances.utils.MessageHelper;
import ru.klesh.finances.utils.async.AsyncTaskWithCallback;
import ru.klesh.finances.utils.async.Callback;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, SearchView.OnQueryTextListener {

    public static final int REQUEST_SING_IN = 1;

    private ReloadTask mReloadTask = null;

    private ContentType contentType = ContentType.PAYMENTS;

    private AppPropertyStorage propertyStorage = AppPropertyStorage.INSTANCE;

    private SwipeRefreshLayout.OnRefreshListener onRefreshListener;

    private PaymentsRepository paymentsRepository = new PaymentsRepository();
    private CategoriesRepository categoriesRepository = new CategoriesRepository();
    private PaymentAccountsRepository accountsRepository = new PaymentAccountsRepository();

    private TextView mMyCashView;
    private SwipeRefreshLayout mSwipeContainer;
    private ListView mContentMainList;
    private View mContentMainListHeaderPlaceholder;
    private ViewSwitcher mContentMainListHeader;
    private View mContentMainHeader;
    private TextView mUserName;
    private TextView mUserPhone;
    private View mContentMainListBackground;
    private FloatingActionButton mFab;

    public MainActivity mInstance;
    private Callback dialogsCallback;
    private Callback tasksCallback;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mInstance = this;

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.

        DrawerLayout sidebarMainDrawer = (DrawerLayout) findViewById(R.id.sidebar_main_layout);
        NavigationView sidebarMainHeader = (NavigationView) findViewById(R.id.sidebar_main_header_view);

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View listHeader = inflater.inflate(R.layout.content_main_header_place, null);

        mFab = (FloatingActionButton) findViewById(R.id.fab);
        mContentMainHeader = findViewById(R.id.content_main_header);
        mContentMainList = (ListView) findViewById(R.id.content_main_list);
        mContentMainListHeader = (ViewSwitcher) findViewById(R.id.content_main_list_header);
        mSwipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        mContentMainListHeaderPlaceholder = listHeader.findViewById(R.id.content_main_list_header_placeholder);
        mContentMainListBackground = findViewById(R.id.content_main_list_background);

        // Assertions checks for layout views
        assert mFab != null;
        assert mSwipeContainer != null;
        assert mContentMainList != null;
        assert sidebarMainDrawer != null;
        assert sidebarMainHeader != null;
        assert mContentMainListHeader != null;
        assert mContentMainListBackground != null;
        assert mContentMainListHeaderPlaceholder != null;

        View sidebarHeaderView = sidebarMainHeader.getHeaderView(0);

        assert sidebarHeaderView != null;

        mUserName = (TextView) sidebarHeaderView.findViewById(R.id.user_name_text);
        mUserPhone = (TextView) sidebarHeaderView.findViewById(R.id.user_phone_text);
        mMyCashView = (TextView) sidebarHeaderView.findViewById(R.id.user_all_cash_text);

        assert mUserName != null;
        assert mUserPhone != null;
        assert mMyCashView != null;

        mContentMainList.addHeaderView(listHeader);
        mContentMainList.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == SCROLL_STATE_IDLE) {
                    mFab.show();
                } else {
                    mFab.hide();
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (mContentMainList.getFirstVisiblePosition() == 0) {
                    View firstChild = mContentMainList.getChildAt(0);
                    int headerChildTopY = 0;
                    if (firstChild != null) {
                        headerChildTopY = firstChild.getTop();
                    }

                    int listHeaderPlaceholderTop = mContentMainListHeaderPlaceholder.getTop();
                    int realHeaderTop = Math.max(0, listHeaderPlaceholderTop + headerChildTopY);

                    mContentMainListHeader.setY(realHeaderTop);
                    mContentMainListBackground.setY(realHeaderTop);
                    mContentMainHeader.setY(headerChildTopY * 0.5f);
                } else {
                    mContentMainListHeader.setY(0);
                    mContentMainListBackground.setY(0);
                }

            }
        });

        onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                startReloadContentTask(new Callback<Boolean>() {
                    @Override
                    public void onCall(Boolean result) {
                        mSwipeContainer.setRefreshing(false);

                        if (!result) {
                            MessageHelper.toastMessage(getBaseContext(), R.string.template_cant_update, "");
                        }
                    }
                });
            }
        };

        mSwipeContainer.setOnRefreshListener(onRefreshListener);
        mSwipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        tasksCallback = new Callback() {
            @Override
            public void onCall(Object o) {
                if (o != null) updateContent();
            }

            public void onError(Throwable result) {
                super.onError(result);
                MessageHelper.toastMessage(getBaseContext(), R.string.template_error, result.toString());
            }
        };


        dialogsCallback = new Callback() {
            @Override
            public void onCall(Object o) {
                if (o != null) reloadContent();
            }

            @Override
            public void onError(Throwable result) {
                super.onError(result);
                MessageHelper.toastMessage(getBaseContext(), R.string.template_error, result.toString());
            }
        };

        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (contentType) {
                    case PAYMENTS:
                        new PaymentDialog(mInstance, dialogsCallback);
                        break;
                    case CATEGORIES:
                        new CategoryDialog(mInstance, dialogsCallback);
                        break;
                    case ACCOUNTS:
                        new AccountDialog(mInstance, dialogsCallback);
                        break;
                }
            }
        });

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, sidebarMainDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        sidebarMainDrawer.setDrawerListener(toggle);
        toggle.syncState();

        sidebarMainHeader.setNavigationItemSelectedListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!FinancesApi.getInstance().isAuthorized()) {
            startActivityForResult(new Intent(this, SingInActivity.class), REQUEST_SING_IN);
            return;
        }

        updateContent();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_SING_IN && resultCode != RESULT_OK) {
            Toast.makeText(this, "Close app", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        reloadContent();
    }


    protected void reloadContent() {
        updateUserData();
        mSwipeContainer.setRefreshing(true);
        onRefreshListener.onRefresh();
    }

    protected void startReloadContentTask(Callback<Boolean> callback) {
        mReloadTask = new ReloadTask();
        mReloadTask.setCallback(callback);
        mReloadTask.execute();
    }

    protected void updateContent() {
        updateUserData();
        switchContent(contentType);
    }

    protected void updateUserData() {
        AppUser appUser = FinancesApi.getInstance().getAppUser();
        mUserPhone.setText(appUser.getPhone());
        mUserName.setText(appUser.getName());
        mMyCashView.setText(String.valueOf(propertyStorage.getLong(FinancesApp.APP_PROP_BALANCE)));
    }

    private void switchContent(ContentType contentType) {
        boolean restoreScroll = this.contentType == contentType;
        this.contentType = contentType;
        switchContentMainList(contentType, restoreScroll);
    }

    @NonNull
    private BaseAdapter createContentAdapter(ContentType contentType) {
        return createContentAdapter(contentType, null);
    }

    @NonNull
    private BaseAdapter createContentAdapter(ContentType contentType, String query) {
        BaseAdapter adapter = null;

        switch (contentType) {
            case PAYMENTS: {
                PaymentListItemAdapter p = new PaymentListItemAdapter(mInstance, paymentsRepository.search(query));
                p.setRemoveItemCallback(new Callback<PaymentDataGroup>() {
                    @Override
                    public void onCall(PaymentDataGroup paymentDataGroup) {
                        if (paymentDataGroup.getPayments().isEmpty()) return;

                        final PaymentData data = paymentDataGroup.getPayments().get(0);
                        String text = getString(R.string.payment) + "\"" + data.getComment() + "\"" + getString(R.string.was_removed);

                        createSnackbar(text, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                data.renew();
                                AddEditPaymentTask task = new AddEditPaymentTask(mInstance, data);
                                task.setCallback(tasksCallback);
                                task.execute();
                            }
                        });
                    }
                });

                adapter = p;
                break;
            }
            case CATEGORIES: {
                CategoriesListItemAdapter c = new CategoriesListItemAdapter(mInstance, categoriesRepository.search(query));
                c.setRemoveItemCallback(new Callback<CategoryData>() {
                    @Override
                    public void onCall(final CategoryData data) {
                        String text = getString(R.string.category) + "\"" + data.getName() + "\"" + getString(R.string.was_removed);

                        createSnackbar(text, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                data.renew();
                                AddEditCategoryTask task = new AddEditCategoryTask(mInstance, data);
                                task.setCallback(tasksCallback);
                                task.execute();
                            }
                        });
                    }
                });

                adapter = c;
                break;
            }
            case ACCOUNTS: {
                AccountsListItemAdapter a = new AccountsListItemAdapter(mInstance, accountsRepository.search(query));
                a.setRemoveItemCallback(new Callback<PaymentAccountData>() {
                    @Override
                    public void onCall(final PaymentAccountData data) {
                        String text = getString(R.string.account) + "\"" + data.getName() + "\"" + getString(R.string.was_removed);

                        createSnackbar(text, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                data.renew();
                                AddEditAccountTask task = new AddEditAccountTask(mInstance, data);
                                task.setCallback(tasksCallback);
                                task.execute();
                            }
                        });
                    }
                });

                adapter = a;
                break;
            }
        }

        adapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                updateContent();
            }
        });

        return adapter;
    }

    private void createSnackbar(String text, View.OnClickListener actionListener) {
        Snackbar snackbar = Snackbar.make(mFab, text, Snackbar.LENGTH_LONG)
                .setAction("Undo", actionListener)
                .setActionTextColor(Color.RED);

        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(Color.DKGRAY);
        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }

    private void switchContentMainList(ContentType contentType, boolean restoreScroll) {
        BaseAdapter adapter = createContentAdapter(contentType);

        int scroll = mContentMainList.getScrollY();

        mContentMainList.setAdapter(adapter);
        mContentMainListHeader.setDisplayedChildId(contentType.ordinal());

        if (restoreScroll) {
            mContentMainList.setScrollY(scroll);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.sidebar_main_layout);
        assert drawer != null;

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);

        //Searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setOnQueryTextListener(this);
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                updateContent();
                return false;
            }
        });
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        BaseAdapter adapter = createContentAdapter(contentType, query);
        mContentMainList.setAdapter(adapter);
        return adapter.getCount() == 0;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_refresh) {
            reloadContent();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_logout_action) {
            propertyStorage.unset(FinancesApp.APP_PROP_PHONE);
            propertyStorage.unset(FinancesApp.APP_PROP_PASSWORD);
            propertyStorage.save();
            FinancesApi.getInstance().setAppUser(null);
            recreate();
        } else if (id == R.id.nav_to_payments) {
            switchContent(ContentType.PAYMENTS);
        } else if (id == R.id.nav_to_categories) {
            switchContent(ContentType.CATEGORIES);
        } else if (id == R.id.nav_to_accounts) {
            switchContent(ContentType.ACCOUNTS);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.sidebar_main_layout);
        assert drawer != null;

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public class ReloadTask extends AsyncTaskWithCallback<Void, Void, Boolean> {
        BalanceEntity mBalanceEntity;

        RichestEntity mRichestEntity;

        @Override
        protected Boolean doInBackground(Void... params) {
            List<Boolean> successes = new ArrayList<>();
            FinancesApi api = FinancesApi.getInstance(getBaseContext());

            mBalanceEntity = api.getBalance();
            mRichestEntity = api.getRichest();

            successes.add(!mBalanceEntity.hasError());
            successes.add(!mRichestEntity.hasError());

            propertyStorage.set(FinancesApp.APP_PROP_BALANCE, mBalanceEntity.getResponseData().getBalance());
            propertyStorage.set(FinancesApp.APP_PROP_RICHEST, mRichestEntity.getResponseData());
            propertyStorage.save();

            try {
                paymentsRepository.reload();
                categoriesRepository.reload();
                accountsRepository.reload();
                successes.add(true);
            } catch (Exception e) {
                successes.add(false);
            }

            return !successes.contains(false);
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mReloadTask = null;

            if (success) {
                updateContent();
            }

            callback(success);
        }

        @Override
        protected void onCancelled() {
            mReloadTask = null;
            callback(false);
        }
    }

    public enum ContentType {
        PAYMENTS, CATEGORIES, ACCOUNTS
    }
}
