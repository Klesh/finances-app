package ru.klesh.finances.lib.tasks;

import android.content.Context;

import ru.klesh.finances.R;
import ru.klesh.finances.api.impl.entity.PaymentAccountsEntity.PaymentAccountData;
import ru.klesh.finances.repository.impl.PaymentAccountsRepository;

public class AddEditAccountTask extends AbstractTask<PaymentAccountData> {

    private final PaymentAccountData mPaymentAccountsEntity;
    private PaymentAccountsRepository accountsRepository = new PaymentAccountsRepository();

    public AddEditAccountTask(Context context, PaymentAccountData categoryData) {
        super(context, R.string.adding_payment_account);
        mPaymentAccountsEntity = categoryData;
    }

    @Override
    protected PaymentAccountData doInBackground(Void... params) {
        try {
            accountsRepository.save(mPaymentAccountsEntity);
            return mPaymentAccountsEntity;
        } catch (Exception e) {
            mError = e;
            return null;
        }
    }
}