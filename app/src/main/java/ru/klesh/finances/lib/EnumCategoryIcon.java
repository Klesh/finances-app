package ru.klesh.finances.lib;

import ru.klesh.finances.R;

/**
 * Created by Klesh on 23.07.2017.
 */

public enum EnumCategoryIcon {
    FOOD(R.drawable.ic_local_dining_black_24dp),
    AIRPLANE(R.drawable.ic_airplanemode_active_black_24dp),
    TRANSPORT(R.drawable.ic_airport_shuttle_black_24dp),
    BOX(R.drawable.ic_archive_black_24dp),
    DOLLAR(R.drawable.ic_attach_money_black_24dp),
    SOUND_NOTE(R.drawable.ic_audiotrack_black_24dp),
    CRESCENT(R.drawable.ic_brightness_3_black_24dp),
    BUSINESS_HOUSE(R.drawable.ic_business_black_24dp),
    CAKE(R.drawable.ic_cake_black_24dp),
    CALL(R.drawable.ic_call_black_24dp),
    CHILD_CAKE(R.drawable.ic_child_care_black_24dp),
    CHILD_FRIENDLY(R.drawable.ic_child_friendly_black_24dp),
    CLOUD(R.drawable.ic_cloud_queue_black_24dp),
    COLOR_LENS(R.drawable.ic_color_lens_black_24dp),
    BIKE(R.drawable.ic_directions_bike_black_24dp),
    CAR(R.drawable.ic_directions_car_black_24dp),
    BUS(R.drawable.ic_directions_bus_black_24dp),
    EV_STATION(R.drawable.ic_ev_station_black_24dp),
    FITNESS(R.drawable.ic_fitness_center_black_24dp),
    PAINT(R.drawable.ic_format_paint_black_24dp),
    BREAKFAST(R.drawable.ic_free_breakfast_black_24dp),
    GOLF_COURSE(R.drawable.ic_golf_course_black_24dp),
    GROUP(R.drawable.ic_group_black_24dp),
    HEADSET(R.drawable.ic_headset_black_24dp),
    HOTEL(R.drawable.ic_hotel_black_24dp),
    BOOK(R.drawable.ic_import_contacts_black_24dp),
    EMOTION(R.drawable.ic_insert_emoticon_black_24dp),
    PHOTO(R.drawable.ic_insert_photo_black_24dp),
    LAPTOP_MAC(R.drawable.ic_laptop_mac_black_24dp),
    DESKTOP_MAC(R.drawable.ic_desktop_mac_black_24dp),
    TABLET_MAC(R.drawable.ic_tablet_mac_black_24dp),
    BAR(R.drawable.ic_local_bar_black_24dp),
    BAR_WASH(R.drawable.ic_local_car_wash_black_24dp),
    DRINK(R.drawable.ic_local_drink_black_24dp),
    GAS_STATION(R.drawable.ic_local_gas_station_black_24dp),
    GROCERY_STONE(R.drawable.ic_local_grocery_store_black_24dp),
    LAUNDRY_SERVICE(R.drawable.ic_local_laundry_service_black_24dp),
    MOVIES(R.drawable.ic_local_movies_black_24dp),
    PARKING(R.drawable.ic_local_parking_black_24dp),
    PHARMACY(R.drawable.ic_local_pharmacy_black_24dp),
    PIZZA(R.drawable.ic_local_pizza_black_24dp),
    SHIPPING(R.drawable.ic_local_shipping_black_24dp),
    TAXI(R.drawable.ic_local_taxi_black_24dp),
    REPAIR(R.drawable.ic_menu_manage),
    MOOD_BAD(R.drawable.ic_mood_bad_black_24dp),
    MOVIE_CREATION(R.drawable.ic_movie_creation_black_24dp),
    NATURE_PEOPLE(R.drawable.ic_nature_people_black_24dp),
    NOTIFICATION(R.drawable.ic_notifications_black_24dp),
    POOL(R.drawable.ic_pool_black_24dp),
    RESTAURANT(R.drawable.ic_restaurant_black_24dp),
    ROUTER(R.drawable.ic_router_black_24dp),
    SCANNER(R.drawable.ic_scanner_black_24dp),
    SCHOOL(R.drawable.ic_school_black_24dp),
    STORAGE(R.drawable.ic_sd_storage_black_24dp),
    SECURITY(R.drawable.ic_security_black_24dp),
    SMOKING(R.drawable.ic_smoking_rooms_black_24dp),
    SPEAKER(R.drawable.ic_speaker_black_24dp),
    SPA(R.drawable.ic_spa_black_24dp),
    STAR(R.drawable.ic_star_black_24dp),
    STORE_MALL(R.drawable.ic_store_mall_directory_black_24dp),
    TRAIN(R.drawable.ic_train_black_24dp),
    INCANDESCENT(R.drawable.ic_wb_incandescent_black_24dp),
    WEEKEND(R.drawable.ic_weekend_black_24dp),
    MONEY_BAG(R.drawable.money_bag),
    YOUTUBE(R.drawable.ic_subscriptions_black_24dp),
    ACCESSIBLE(R.drawable.ic_accessible_black_24px),
    CARD_MEMBERSHIP(R.drawable.ic_card_membership_black_24px),
    CREDIT_CARD(R.drawable.ic_credit_card_black_24dp),
    EXPLORE(R.drawable.ic_explore_black_24px),
    FACE(R.drawable.ic_face_black_24px),
    FAVORITE(R.drawable.ic_favorite_black_24px),
    HOME(R.drawable.ic_home_black_24px),
    LANGUAGE(R.drawable.ic_language_black_24px),
    LIGHTBULB_OUTLINE(R.drawable.ic_lightbulb_outline_black_24px),
    MOTORCYCLE(R.drawable.ic_motorcycle_black_24px),
    PERM_PHONE_MSG(R.drawable.ic_perm_phone_msg_black_24px),
    PETS(R.drawable.ic_pets_black_24px),
    REDEEM(R.drawable.ic_redeem_black_24px),
    WORK(R.drawable.ic_work_black_24px),
    MONEY_OFF(R.drawable.ic_money_off_black_24dp),
    MONEY(R.drawable.ic_monetization_on_black_24dp),
    UNKNOWN(R.drawable.ic_error_black_24dp);

    private int resId;

    EnumCategoryIcon(int resId) {
        this.resId = resId;
    }

    public int getResId() {
        return resId;
    }

    public static EnumCategoryIcon safeValueOf(String name) {
        try {
            return valueOf(name);
        } catch (IllegalArgumentException ignored) {
            return UNKNOWN;
        }
    }
}
