package ru.klesh.finances.lib.adapters;

import android.content.Context;

import java.util.Collection;

import ru.klesh.finances.api.impl.entity.PaymentAccountsEntity.PaymentAccountData;

/**
 * Created by Klesh on 17.01.2017.
 */

public class SpinnerAccountsAdapter extends SpinnerDataAdapter<PaymentAccountData> {

    public SpinnerAccountsAdapter(Context context, Collection<PaymentAccountData> data) {
        super(context, data);
    }

    @Override
    protected String getText(PaymentAccountData item) {
        return item != null ? item.getName() : "null";
    }
}
