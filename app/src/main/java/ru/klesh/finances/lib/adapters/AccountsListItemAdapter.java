package ru.klesh.finances.lib.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import ru.klesh.finances.R;
import ru.klesh.finances.api.impl.entity.PaymentAccountsEntity.PaymentAccountData;
import ru.klesh.finances.lib.dialogs.AccountDialog;
import ru.klesh.finances.lib.dialogs.ExtraResultDialog;
import ru.klesh.finances.lib.dialogs.PaymentDialog;
import ru.klesh.finances.lib.tasks.RemovePaymentAccountTask;
import ru.klesh.finances.utils.MessageHelper;
import ru.klesh.finances.utils.async.Callback;
import ru.klesh.finances.utils.compact.CompactUtils;

public class AccountsListItemAdapter extends BaseItemAdapter<PaymentAccountData> {

    private int colorRowOdd;

    public AccountsListItemAdapter(Context context, Collection<PaymentAccountData> data) {
        super(context, R.layout.list_item_account);
        colorRowOdd = CompactUtils.getColorFromResources(context, R.color.colorRowOdd);

        List<PaymentAccountData> categories = new ArrayList<>(data);
        setData(categories);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = super.getView(position, convertView, parent);

        final PaymentAccountData data = this.getData().get(position);
        final TextView name = (TextView) vi.findViewById(R.id.account_list_item_name);
        final TextView currencyName = (TextView) vi.findViewById(R.id.account_list_item_currency);
        final View background = vi.findViewById(R.id.list_item_background);
        final ImageButton editBtn = (ImageButton) vi.findViewById(R.id.list_item_payment_account_edit_btn);
        final ImageButton deleteBtn = (ImageButton) vi.findViewById(R.id.list_item_payment_account_delete_btn);
        final SwipeLayout swipeLayout = (SwipeLayout) vi.findViewById(R.id.list_item_payment_account_swipe_layout);

        assert name != null;
        assert editBtn != null;
        assert deleteBtn != null;
        assert background != null;
        assert swipeLayout != null;
        assert currencyName != null;

        name.setText(data.getName());
        currencyName.setText(data.getCurrency().getNameStrResId());
        background.setBackgroundColor(position % 2 == 0 ? colorRowOdd : 0);

        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                swipeLayout.close();
                editClick(data);
            }
        });

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                swipeLayout.close();
                removeClick(data);
            }
        });

        swipeLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                swipeLayout.toggle();
                return false;
            }
        });

        return vi;
    }

    private void editClick(PaymentAccountData accountData) {
        new AccountDialog(context, new Callback<ExtraResultDialog.Result>() {
            @Override
            public void onCall(ExtraResultDialog.Result result) {
                PaymentAccountData accountData = result.get(AccountDialog.KEY_PAYMENT_ACCOUNT_DATA);
                int actionType = result.get(PaymentDialog.KEY_ACTION_TYPE);

                if (accountData == null || actionType < 0) {
                    return;
                }

                if (actionType == PaymentDialog.ACTION_TYPE_ADD) {
                    getData().add(accountData);
                } else if (actionType == PaymentDialog.ACTION_TYPE_REMOVE) {
                    getData().remove(accountData);
                }

                notifyDataSetChanged();
            }
        }, accountData);
    }

    private void removeClick(PaymentAccountData accountData) {
        if (accountData != null) {
            RemovePaymentAccountTask task = new RemovePaymentAccountTask(context, accountData.getId());
            task.setCallback(new Callback<PaymentAccountData>() {
                public void onCall(PaymentAccountData result) {
                    notifyDataItemRemoved(result);
                }

                @Override
                public void onError(Throwable result) {
                    super.onError(result);
                    MessageHelper.toastMessage(context, R.string.template_cant_remove_payment_account, result.toString());
                }
            });
            task.execute();
        }
    }

}