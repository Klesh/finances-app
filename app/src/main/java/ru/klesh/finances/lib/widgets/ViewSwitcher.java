package ru.klesh.finances.lib.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

import ru.klesh.finances.R;

/**
 * Created by Klesh on 17.01.2017.
 */

public class ViewSwitcher extends FrameLayout {

    private int mDisplayedChildId = 0;

    public ViewSwitcher(Context context) {
        super(context);
        init(null);
    }

    public ViewSwitcher(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public ViewSwitcher(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ViewSwitcher(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        if (attrs == null) {
            return;
        }

        TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.ViewSwitcher, 0, 0);
        try {
            mDisplayedChildId = ta.getInteger(R.styleable.ViewSwitcher_displayed_child_id, 0);
        } finally {
            ta.recycle();
        }

        setDisplayedChildId(mDisplayedChildId);
    }

    public void setDisplayedChildId(int displayedChildId) {
        this.mDisplayedChildId = displayedChildId;

        for (int i = 0; i < getChildCount(); i++) {
            View childView = getChildAt(i);
            if (i == displayedChildId) {
                childView.setVisibility(VISIBLE);
            } else {
                childView.setVisibility(INVISIBLE);
            }
        }

        invalidate();
        requestLayout();
    }

    public int getDisplayedChildId() {
        return mDisplayedChildId;
    }
}
