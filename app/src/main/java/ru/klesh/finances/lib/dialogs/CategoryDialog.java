package ru.klesh.finances.lib.dialogs;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import ru.klesh.finances.R;
import ru.klesh.finances.api.impl.entity.CategoriesEntity.CategoryData;
import ru.klesh.finances.lib.EnumCategoryIcon;
import ru.klesh.finances.lib.tasks.AddEditCategoryTask;
import ru.klesh.finances.repository.RepositoryException;
import ru.klesh.finances.repository.impl.CategoryIconRepository;
import ru.klesh.finances.repository.impl.CategoryIconRepository.CategoryIconData;
import ru.klesh.finances.utils.MessageHelper;
import ru.klesh.finances.utils.async.Callback;

/**
 * Created by Klesh on 12.01.2017.
 */

public class CategoryDialog extends ExtraResultDialog implements MaterialDialog.SingleButtonCallback {

    public static final String KEY_CATEGORY_DATA = "category";
    public static final String KEY_ACTION_TYPE = "actType";

    public static final int ACTION_TYPE_EDIT = 0;
    public static final int ACTION_TYPE_ADD = 1;
    public static final int ACTION_TYPE_REMOVE = 2;

    private TextView mName;
    private ImageButton mIconButton;

    private CategoryData mCategoryData;
    private CategoryIconData mCategoryIconDataData;

    private CategoryIconRepository iconRepository = new CategoryIconRepository();

    public CategoryDialog(@NonNull Context context, Callback<Result> callback) {
        this(context, callback, null);
    }

    public CategoryDialog(@NonNull final Context context, Callback<Result> callback, CategoryData presetData) {
        super(context, callback);
        title(presetData == null ? R.string.add_category : R.string.edit_category);
        customView(R.layout.dialog_add_category, true);
        positiveText(presetData == null ? R.string.add_text : R.string.save);
        negativeText(R.string.cancel);
        autoDismiss(false);
        onAny(this);
        show();

        mCategoryData = presetData;

        mName = (TextView) customView.findViewById(R.id.dialog_add_category_name);
        mIconButton = (ImageButton) customView.findViewById(R.id.dialog_add_category_icon);

        assert mName != null;
        assert mIconButton != null;

        if (mCategoryData != null) {
            mName.setText(mCategoryData.getName());
        }

        mCategoryIconDataData = iconRepository.get(mCategoryData.getId());
        if (mCategoryIconDataData == null) {
            mCategoryIconDataData = new CategoryIconData(mCategoryData.getId(), EnumCategoryIcon.UNKNOWN.name());
        }

        EnumCategoryIcon icon = EnumCategoryIcon.safeValueOf(mCategoryIconDataData.getIcon());
        mIconButton.setImageResource(icon.getResId());

        mIconButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new CategoryIconDialog(context, new Callback<Result>() {
                    @Override
                    public void onCall(Result result) {
                        EnumCategoryIcon icon = result.get(CategoryIconDialog.KEY_CATEGORY_ICON_DATA);
                        if (icon != null) {
                            mCategoryIconDataData.setIcon(icon.name());
                            mIconButton.setImageResource(icon.getResId());
                        }
                    }
                });
            }
        });
    }

    @Override
    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
        switch (which) {
            case POSITIVE:
                addSaveClick();
                break;
            default:
                instance.hide();
        }
    }

    private void addSaveClick() {
        setResult(KEY_ACTION_TYPE, ACTION_TYPE_EDIT);

        if (mCategoryData == null) {
            setResult(KEY_ACTION_TYPE, ACTION_TYPE_ADD);
            mCategoryData = new CategoryData();
        }

        if (mName.getText().length() == 0) {
            mName.setError(context.getString(R.string.enter_valid_value));
            mName.requestFocus();
            return;
        }

        try {
            mCategoryData.setName(mName.getText().toString());
        } catch (NumberFormatException e) {
            mName.setError(context.getString(R.string.enter_valid_value));
            mName.requestFocus();
            return;
        }

        try {
            if (mCategoryIconDataData != null) {
                iconRepository.save(mCategoryIconDataData);
            }
        } catch (RepositoryException e) {
            MessageHelper.toastMessage(context, R.string.template_cant_save_cat_icon, e.toString());
            e.printStackTrace();
        }

        AddEditCategoryTask task = new AddEditCategoryTask(context, mCategoryData);
        task.setCallback(new Callback<CategoryData>() {
            @Override
            public void onCall(CategoryData paymentData) {
                setResult(KEY_CATEGORY_DATA, paymentData);
                instance.hide();
                finishDialog();
            }

            @Override
            public void onError(Throwable result) {
                super.onError(result);
                MessageHelper.toastMessage(context, R.string.template_cant_process_category, result.toString());
            }
        });
        task.execute();
    }
}
