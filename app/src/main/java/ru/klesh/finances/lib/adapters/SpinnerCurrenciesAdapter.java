package ru.klesh.finances.lib.adapters;

import android.content.Context;

import java.util.Collection;

import ru.klesh.finances.api.EnumCurrency;

/**
 * Created by Klesh on 17.01.2017.
 */

public class SpinnerCurrenciesAdapter extends SpinnerDataAdapter<EnumCurrency> {

    public SpinnerCurrenciesAdapter(Context context, Collection<EnumCurrency> data) {
        super(context, data);
    }

    @Override
    protected String getText(EnumCurrency item) {
        String name = getContext().getString(item.getNameStrResId());
        String symbol = getContext().getString(item.getSymbolStrResId());
        return name + " " + symbol;
    }
}
