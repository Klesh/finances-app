package ru.klesh.finances.lib.adapters;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import ru.klesh.finances.FinancesApp;
import ru.klesh.finances.R;
import ru.klesh.finances.api.impl.entity.PaymentsEntity.PaymentData;
import ru.klesh.finances.utils.DateFormatter;
import ru.klesh.finances.utils.async.Callback;
import ru.klesh.finances.utils.http.DateHelper;

public class PaymentListItemAdapter extends HolderItemAdapter<PaymentListItemAdapter.PaymentDataGroup, PaymentListItemAdapter.ViewHolder> {

    public PaymentListItemAdapter(Context context, Collection<PaymentData> data) {
        super(context, R.layout.list_item_payment_group);
        List<PaymentData> dataCopy = new ArrayList<>(data);
        List<PaymentDataGroup> groups = new ArrayList<>();

        Collections.sort(dataCopy, PaymentData.PAYMENT_DATA_COMPARATOR);

        while (!dataCopy.isEmpty()) {
            long time = -1L;
            int income = 0;
            int outcome = 0;
            List<PaymentData> sub = new ArrayList<>();
            Iterator<PaymentData> iterator = dataCopy.iterator();

            while (iterator.hasNext()) {
                PaymentData next = iterator.next();

                if (time == -1L) {
                    time = DateHelper.removeTimePart(next.getTime());
                }

                if (DateHelper.removeTimePart(next.getTime()) == time) {
                    int price = next.getValue();
                    if (price < 0) {
                        outcome += price;
                    } else {
                        income += price;
                    }

                    sub.add(next);
                    iterator.remove();
                }
            }

            PaymentDataGroup group = new PaymentDataGroup(sub, income, outcome, time);
            PaymentSubListItemAdapter subAdapter = new PaymentSubListItemAdapter(context, sub);
            group.setAdapter(subAdapter);
            groups.add(group);

            subAdapter.setRemoveItemCallback(new Callback<PaymentData>() {
                @Override
                public void onCall(PaymentData paymentData) {
                    ArrayList<PaymentData> payments = new ArrayList<>();
                    payments.add(paymentData);
                    removeCallback.onCall(new PaymentDataGroup(payments, 0, 0, 0));
                }
            });

            subAdapter.registerDataSetObserver(new DataSetObserver() {
                @Override
                public void onChanged() {
                    notifyDataSetChanged();
                }
            });
        }

        setData(groups);
    }

    @Override
    protected ViewHolder createHolder(View vi) {
        ViewHolder holder = new ViewHolder();
        holder.income = (TextView) vi.findViewById(R.id.payment_list_item_group_income);
        holder.outcome = (TextView) vi.findViewById(R.id.payment_list_item_group_outcome);
        holder.date_dd = (TextView) vi.findViewById(R.id.payment_list_item_group_date_dd);
        holder.date_mm_yyyy = (TextView) vi.findViewById(R.id.payment_list_item_group_date_mm_yyy);
        holder.date_day = (TextView) vi.findViewById(R.id.payment_list_item_group_date_day);
        holder.subList = (LinearLayout) vi.findViewById(R.id.payment_list_item_sub_items_holder);

        assert holder.income != null;
        assert holder.outcome != null;
        assert holder.date_dd != null;
        assert holder.date_mm_yyyy != null;
        assert holder.date_day != null;
        assert holder.subList != null;

        return holder;
    }

    @Override
    protected void actualizeHolder(int position, ViewHolder holder, ViewGroup parent) {
        final PaymentDataGroup data = this.getData().get(position);

        if (data.getIncome() > 0) {
            String text = String.valueOf(data.getIncome()) + FinancesApp.CURRENCY_SYMBOL;
            holder.income.setText(text);
            holder.income.setVisibility(View.VISIBLE);
        } else {
            holder.income.setVisibility(View.GONE);
        }

        if (data.getOutcome() < 0) {
            String text = String.valueOf(data.getOutcome()) + FinancesApp.CURRENCY_SYMBOL;
            holder.outcome.setText(text);
            holder.outcome.setVisibility(View.VISIBLE);
        } else {
            holder.outcome.setVisibility(View.GONE);
        }

        holder.date_dd.setText(DateFormatter.format(data.getTime(), "dd"));
        holder.date_mm_yyyy.setText(DateFormatter.format(data.getTime(), "MM.yyyy"));
        holder.date_day.setText(DateFormatter.format(data.getTime(), "EEEE"));

        ListAdapter adapter = data.getAdapter();
        int count = Math.max(adapter.getCount(), holder.subList.getChildCount());
        for (int i = 0; i < count; i++) {
            View existed = holder.subList.getChildAt(i);

            if (i < adapter.getCount()) {
                View child = adapter.getView(i, existed, null);
                if (existed == null) {
                    holder.subList.addView(child);
                } else {
                    existed.setVisibility(View.VISIBLE);
                }
            } else {
                existed.setVisibility(View.GONE);
            }
        }
    }

    public static class PaymentDataGroup {
        private List<PaymentData> payments;
        private int income;
        private int outcome;
        private long time;
        private ListAdapter adapter;

        PaymentDataGroup(List<PaymentData> payments, int income, int outcome, long time) {
            this.payments = payments;
            this.income = income;
            this.outcome = outcome;
            this.time = time;
        }

        void setAdapter(ListAdapter adapter) {
            this.adapter = adapter;
        }

        long getTime() {
            return time;
        }

        ListAdapter getAdapter() {
            return adapter;
        }

        public List<PaymentData> getPayments() {
            return payments;
        }

        int getIncome() {
            return income;
        }

        int getOutcome() {
            return outcome;
        }
    }

    static class ViewHolder {
        TextView income;
        TextView outcome;
        TextView date_dd;
        TextView date_mm_yyyy;
        TextView date_day;
        LinearLayout subList;
    }
}
