package ru.klesh.finances.lib.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import ru.klesh.finances.utils.async.Callback;

public abstract class BaseItemAdapter<T> extends BaseAdapter {

    protected Context context;
    protected Callback<T> removeCallback;
    private List<T> data = new ArrayList<>();

    private static LayoutInflater inflater = null;
    private int layoutResId;

    public BaseItemAdapter(Context context, int layoutResId) {
        this.context = context;
        this.layoutResId = layoutResId;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public BaseItemAdapter(Context context, int layoutResId, Collection<T> data) {
        this(context, layoutResId);
        setData(data);
    }

    public void setData(Collection<T> data) {
        this.data = new ArrayList<>(data);
    }

    public List<T> getData() {
        return data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (vi == null)
            vi = inflater.inflate(this.layoutResId, null);
        return vi;
    }

    public void setRemoveItemCallback(Callback<T> removeCallback) {
        this.removeCallback = removeCallback;
    }

    protected void notifyDataItemRemoved(T item) {
        notifyDataSetChanged();

        if (removeCallback != null) {
            removeCallback.onCall(item);
        }
    }
}