package ru.klesh.finances.lib.tasks;

import android.content.Context;

import ru.klesh.finances.R;
import ru.klesh.finances.api.impl.entity.PaymentsEntity.PaymentData;
import ru.klesh.finances.repository.impl.PaymentsRepository;

public class RemovePaymentTask extends AbstractTask<PaymentData> {

    private int mPaymentId;
    private PaymentsRepository paymentsRepository = new PaymentsRepository();

    public RemovePaymentTask(Context context, int id) {
        super(context, R.string.removing_payment);
        mPaymentId = id;
    }

    @Override
    protected PaymentData doInBackground(Void... params) {
        try {
            return paymentsRepository.remove(mPaymentId);
        } catch (Exception e) {
            mError = e;
            return null;
        }
    }
}