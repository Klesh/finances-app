package ru.klesh.finances.lib.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ru.klesh.finances.FinancesApp;
import ru.klesh.finances.R;
import ru.klesh.finances.api.impl.entity.CategoriesEntity.CategoryData;
import ru.klesh.finances.lib.EnumCategoryIcon;
import ru.klesh.finances.lib.dialogs.CategoryDialog;
import ru.klesh.finances.lib.dialogs.ExtraResultDialog;
import ru.klesh.finances.lib.dialogs.PaymentDialog;
import ru.klesh.finances.lib.tasks.RemoveCategoryTask;
import ru.klesh.finances.repository.impl.CategoryIconRepository;
import ru.klesh.finances.utils.MessageHelper;
import ru.klesh.finances.utils.async.Callback;
import ru.klesh.finances.utils.compact.CompactUtils;

public class CategoriesListItemAdapter extends BaseItemAdapter<CategoryData> {

    private int colorRowOdd;

    private static CategoryIconRepository categoryIconRepository = new CategoryIconRepository();

    public CategoriesListItemAdapter(Context context, Collection<CategoryData> data) {
        super(context, R.layout.list_item_category);
        colorRowOdd = CompactUtils.getColorFromResources(context, R.color.colorRowOdd);

        Comparator<CategoryData> categoryDataComparator = new Comparator<CategoryData>() {
            @Override
            public int compare(CategoryData t0, CategoryData t1) {
                return (int) (t1.getTotalMoney() - t0.getTotalMoney());
            }
        };

        List<CategoryData> categories = new ArrayList<>(data);
        Collections.sort(categories, categoryDataComparator);
        setData(categories);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = super.getView(position, convertView, parent);

        final CategoryData data = this.getData().get(position);
        final TextView name = (TextView) vi.findViewById(R.id.category_list_item_name);
        final TextView totalValue = (TextView) vi.findViewById(R.id.category_list_item_total_value);
        final View background = vi.findViewById(R.id.list_item_background);
        final ImageButton editBtn = (ImageButton) vi.findViewById(R.id.list_item_category_edit_btn);
        final ImageButton deleteBtn = (ImageButton) vi.findViewById(R.id.list_item_category_delete_btn);
        final ImageView image = (ImageView) vi.findViewById(R.id.category_list_item_image);
        final SwipeLayout swipeLayout = (SwipeLayout) vi.findViewById(R.id.list_item_category_swipe_layout);

        assert name != null;
        assert background != null;
        assert totalValue != null;
        assert editBtn != null;
        assert deleteBtn != null;
        assert image != null;
        assert swipeLayout != null;

        String money = String.valueOf(data.getTotalMoney()) + FinancesApp.CURRENCY_SYMBOL;

        name.setText(data.getName());
        totalValue.setText(money);

        background.setBackgroundColor(position % 2 == 0 ? colorRowOdd : 0);

        CategoryIconRepository.CategoryIconData iconData = categoryIconRepository.get(data.getId());
        if (iconData == null) {
            iconData = new CategoryIconRepository.CategoryIconData(data.getId(), EnumCategoryIcon.UNKNOWN.name());
        }

        EnumCategoryIcon icon = EnumCategoryIcon.safeValueOf(iconData.getIcon());
        image.setImageResource(icon.getResId());

        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                swipeLayout.close();
                editClick(data);
            }
        });

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                swipeLayout.close();
                removeClick(data);
            }
        });

        swipeLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                swipeLayout.toggle();
                return false;
            }
        });

        return vi;
    }

    private void editClick(CategoryData categoryData) {
        new CategoryDialog(context, new Callback<ExtraResultDialog.Result>() {
            @Override
            public void onCall(ExtraResultDialog.Result result) {
                CategoryData categoryData = result.get(CategoryDialog.KEY_CATEGORY_DATA);
                int actionType = result.get(PaymentDialog.KEY_ACTION_TYPE);

                if (categoryData == null || actionType < 0) {
                    return;
                }

                if (actionType == PaymentDialog.ACTION_TYPE_ADD) {
                    getData().add(categoryData);
                } else if (actionType == PaymentDialog.ACTION_TYPE_REMOVE) {
                    getData().remove(categoryData);
                }

                notifyDataSetChanged();
            }
        }, categoryData);
    }

    private void removeClick(CategoryData categoryData) {
        if (categoryData != null) {
            RemoveCategoryTask task = new RemoveCategoryTask(context, categoryData.getId());
            task.setCallback(new Callback<CategoryData>() {
                public void onCall(CategoryData result) {
                    notifyDataItemRemoved(result);
                }

                @Override
                public void onError(Throwable result) {
                    super.onError(result);
                    MessageHelper.toastMessage(context, R.string.template_cant_remove_category, result.toString());
                }
            });
            task.execute();
        }
    }
}