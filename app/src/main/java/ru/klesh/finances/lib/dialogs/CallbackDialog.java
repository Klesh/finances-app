package ru.klesh.finances.lib.dialogs;

import android.content.Context;

import ru.klesh.finances.utils.async.Callback;

/**
 * Created by Klesh on 17.01.2017.
 */

public abstract class CallbackDialog<Result> extends BaseMaterialDialog {

    private Callback<Result> callback;

    public CallbackDialog(Context context, Callback<Result> callback) {
        super(context);
        this.callback = callback;
    }

    protected void callCallback(Result result) {
        if (callback != null) {
            callback.onCall(result);
        }
    }
}
