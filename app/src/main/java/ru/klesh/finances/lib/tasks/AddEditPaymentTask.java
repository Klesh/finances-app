package ru.klesh.finances.lib.tasks;

import android.content.Context;

import ru.klesh.finances.R;
import ru.klesh.finances.api.impl.entity.PaymentsEntity.PaymentData;
import ru.klesh.finances.repository.impl.PaymentsRepository;

public class AddEditPaymentTask extends AbstractTask<PaymentData> {

    private PaymentData mTaskPaymentData;
    private PaymentsRepository paymentsRepository = new PaymentsRepository();

    public AddEditPaymentTask(Context context, PaymentData paymentData) {
        super(context, paymentData.isNew() ? R.string.adding_payment : R.string.editing_payment);
        mTaskPaymentData = paymentData;
    }

    @Override
    protected PaymentData doInBackground(Void... params) {
        try {
            paymentsRepository.save(mTaskPaymentData);
            return mTaskPaymentData;
        } catch (Exception e) {
            mError = e;
            return null;
        }
    }
}