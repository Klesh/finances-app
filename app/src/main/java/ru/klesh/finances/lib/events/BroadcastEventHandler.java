package ru.klesh.finances.lib.events;

/**
 * Created by Klesh on 23.07.2017.
 */

public abstract class BroadcastEventHandler<E extends IBroadcastEvent> {
    private Object owner;
    private Class<? extends IBroadcastEvent> eventClass;

    public BroadcastEventHandler(Object owner, Class<? extends IBroadcastEvent> eventClass) {
        this.owner = owner;
        this.eventClass = eventClass;
    }

    public abstract void onEvent(E event);

    public Class<? extends IBroadcastEvent> getEventClass() {
        return eventClass;
    }

    public Object getOwner() {
        return owner;
    }
}
