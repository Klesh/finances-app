package ru.klesh.finances.lib.adapters;

import android.content.Context;

import java.util.Collection;

import ru.klesh.finances.api.impl.entity.CategoriesEntity.CategoryData;

/**
 * Created by Klesh on 17.01.2017.
 */

public class SpinnerCategoriesAdapter extends SpinnerDataAdapter<CategoryData> {

    public SpinnerCategoriesAdapter(Context context, Collection<CategoryData> data) {
        super(context, data);
    }

    @Override
    protected String getText(CategoryData item) {
        return item != null ? item.getName() : "null";
    }
}
