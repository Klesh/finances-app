package ru.klesh.finances.lib.tasks;

import android.content.Context;

import ru.klesh.finances.R;
import ru.klesh.finances.api.impl.entity.PaymentAccountsEntity.PaymentAccountData;
import ru.klesh.finances.repository.impl.PaymentAccountsRepository;

public class RemovePaymentAccountTask extends AbstractTask<PaymentAccountData> {

    private int mPaymentAccountId;
    private PaymentAccountsRepository accountsRepository = new PaymentAccountsRepository();

    public RemovePaymentAccountTask(Context context, int id) {
        super(context, R.string.remove_payment_account);
        mPaymentAccountId = id;
    }

    @Override
    protected PaymentAccountData doInBackground(Void... params) {
        try {
            return accountsRepository.remove(mPaymentAccountId);
        } catch (Exception e) {
            mError = e;
            return null;
        }
    }
}