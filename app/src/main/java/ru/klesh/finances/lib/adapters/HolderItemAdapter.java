package ru.klesh.finances.lib.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import java.util.Collection;

public abstract class HolderItemAdapter<T, H> extends BaseItemAdapter<T> {


    public HolderItemAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }

    public HolderItemAdapter(Context context, int layoutResId, Collection<T> data) {
        super(context, layoutResId, data);
    }

    protected abstract H createHolder(View v);

    protected abstract void actualizeHolder(int position, H h, ViewGroup parent);

    @Override
    @SuppressWarnings("unchecked")
    public final View getView(int position, View convertView, ViewGroup parent) {
        View vi = super.getView(position, convertView, parent);

        H holder = (H) vi.getTag();
        if (holder == null) {
            holder = createHolder(vi);
            vi.setTag(holder);
        }

        actualizeHolder(position, holder, parent);

        return vi;
    }
}