package ru.klesh.finances.lib.tasks;

import android.content.Context;

import ru.klesh.finances.R;
import ru.klesh.finances.api.impl.entity.CategoriesEntity.CategoryData;
import ru.klesh.finances.repository.impl.CategoriesRepository;

public class AddEditCategoryTask extends AbstractTask<CategoryData> {

    private final CategoryData mCategoryData;
    private CategoriesRepository categoriesRepository = new CategoriesRepository();

    public AddEditCategoryTask(Context context, CategoryData categoryData) {
        super(context, R.string.adding_category);
        mCategoryData = categoryData;
    }

    @Override
    protected CategoryData doInBackground(Void... params) {
        try {
            categoriesRepository.save(mCategoryData);
            return mCategoryData;
        } catch (Exception e) {
            mError = e;
            return null;
        }
    }
}