package ru.klesh.finances.api.impl.entity;

import ru.klesh.finances.api.entity.Entity;

/**
 * Created by Klesh on 16.01.2017.
 */

public class PaymentsFilterEntity implements Entity {

    private PaymentsFilterData filter;

    public PaymentsFilterData getFilter() {
        return filter;
    }

    public void setFilter(PaymentsFilterData filter) {
        this.filter = filter;
    }

    private class PaymentsFilterData implements Entity {
        private Long timeFrom;
        private Long timeTo;
        private Long moneyFrom;
        private Long moneyTo;
        private Long categoryId;
        private String comment;

        public Long getTimeFrom() {
            return timeFrom;
        }

        public void setTimeFrom(Long timeFrom) {
            this.timeFrom = timeFrom;
        }

        public Long getTimeTo() {
            return timeTo;
        }

        public void setTimeTo(Long timeTo) {
            this.timeTo = timeTo;
        }

        public Long getMoneyFrom() {
            return moneyFrom;
        }

        public void setMoneyFrom(Long moneyFrom) {
            this.moneyFrom = moneyFrom;
        }

        public Long getMoneyTo() {
            return moneyTo;
        }

        public void setMoneyTo(Long moneyTo) {
            this.moneyTo = moneyTo;
        }

        public Long getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(Long categoryId) {
            this.categoryId = categoryId;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        @Override
        public String toString() {
            return String.format("PaymentsFilterEntity : {" +
                            "timeFrom = %s, " +
                            "timeTo = %s, " +
                            "moneyFrom = %s, " +
                            "moneyTo = %s, " +
                            "categoryId = %s, " +
                            "comment = %s" +
                            "}",
                    timeFrom,
                    timeTo,
                    moneyFrom,
                    moneyTo,
                    categoryId,
                    comment
            );
        }
    }
}
