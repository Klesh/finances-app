package ru.klesh.finances.api.entity;

/**
 * Created by klesh on 05.01.17.
 */

public abstract class RepositoryEntity<ID_TYPE> implements Entity {

    protected ID_TYPE id;

    public ID_TYPE getId() {
        return id;
    }

    public void setId(ID_TYPE id) {
        this.id = id;
    }

    public boolean isNew() {
        return id == null;
    }

    public void renew() {
        setId(null);
    }

    @Override
    public boolean equals(Object o) {
        if (super.equals(o)) {
            return true;
        }

        if (o == null) {
            return false;
        }

        if (!(o instanceof RepositoryEntity)) {
            return false;
        }

        RepositoryEntity entity = (RepositoryEntity) o;
        return entity.getId().equals(id);
    }
}
