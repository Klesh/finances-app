package ru.klesh.finances.api.entity;

/**
 * Created by klesh on 05.01.17.
 */

public class IdEntity extends ResponseEntity<IdEntity.IdData> {

    public static class IdData implements Entity {

        private Integer id;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }
    }
}
