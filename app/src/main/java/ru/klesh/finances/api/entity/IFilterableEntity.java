package ru.klesh.finances.api.entity;

import java.util.Collection;

/**
 * Created by Klesh on 23.01.2017.
 */

public interface IFilterableEntity {
    boolean canSearch(String search);
}
