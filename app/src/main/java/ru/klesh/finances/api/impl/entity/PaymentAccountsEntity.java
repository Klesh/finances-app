package ru.klesh.finances.api.impl.entity;

import com.google.gson.annotations.SerializedName;

import ru.klesh.finances.api.EnumCurrency;
import ru.klesh.finances.api.entity.IAggregableEntity;
import ru.klesh.finances.api.entity.IFilterableEntity;
import ru.klesh.finances.api.entity.RepositoryEntity;
import ru.klesh.finances.api.entity.ResponseEntity;

/**
 * Created by klesh on 05.01.17.
 */

public class PaymentAccountsEntity extends ResponseEntity<PaymentAccountsEntity.PaymentAccountData[]> {

    public static class PaymentAccountData extends RepositoryEntity<Integer> implements IFilterableEntity, IAggregableEntity<PaymentAccountData> {

        public static final String AGGREGATOR_TYPE_DATE = "currency";

        private String name;

        @SerializedName("currency_id")
        private Integer currencyId;

        @SerializedName("user_id")
        private Integer userId;

        private transient EnumCurrency currency = null;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getCurrencyId() {
            return currencyId;
        }

        public EnumCurrency getCurrency() {
            if (currency == null || currency.getId() != currencyId) {
                currency = EnumCurrency.getById(currencyId);
            }

            return currency;
        }

        public void setCurrencyId(Integer currencyId) {
            this.currencyId = currencyId;
        }

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        @Override
        public String toString() {
            return String.format(
                    "PaymentsEntity: { " +
                            "id = %s, " +
                            "name = %s, " +
                            "currencyId = %s, " +
                            "userId = %s }",
                    id,
                    name,
                    currencyId,
                    userId
            );
        }

        @Override
        public boolean canSearch(String search) {
            if (getName().toLowerCase().contains(search)) {
                return true;
            }

            if (getName().toLowerCase().contains(search)) {
                return true;
            }

            return false;
        }

        @Override
        public boolean canAggregate(String type, PaymentAccountData entity) {
            switch (type) {
                case AGGREGATOR_TYPE_DATE:
                    return entity.getCurrencyId().equals(getCurrencyId());
            }
            return false;
        }
    }


}
