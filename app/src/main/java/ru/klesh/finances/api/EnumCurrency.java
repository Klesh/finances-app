package ru.klesh.finances.api;

import ru.klesh.finances.R;

/**
 * Created by Klesh on 21.07.2017.
 */

public enum EnumCurrency {
    RUBLES(R.string.currency_name_rubles, R.string.currency_symbol_rub, 1),
    UNKNOWN(R.string.unknown, R.string.unknown, -1);

    private final int nameStrResId;
    private final int symbolStrResId;
    private final int id;

    EnumCurrency(int nameStrResId, int symbolStrResId, int id) {
        this.nameStrResId = nameStrResId;
        this.symbolStrResId = symbolStrResId;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public int getNameStrResId() {
        return nameStrResId;
    }

    public int getSymbolStrResId() {
        return symbolStrResId;
    }

    public static EnumCurrency getById(int id) {
        for (EnumCurrency c : values()) {
            if (c.getId() == id) {
                return c;
            }
        }

        return UNKNOWN;
    }
}
