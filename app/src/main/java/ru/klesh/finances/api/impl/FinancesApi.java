package ru.klesh.finances.api.impl;

import android.content.Context;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.util.Log;

import java.lang.ref.WeakReference;

import ru.klesh.finances.R;
import ru.klesh.finances.api.IFinancesApi;
import ru.klesh.finances.api.NotAuthorizedException;
import ru.klesh.finances.api.entity.IdEntity;
import ru.klesh.finances.api.entity.ResponseEntity;
import ru.klesh.finances.api.impl.entity.BalanceEntity;
import ru.klesh.finances.api.impl.entity.CategoriesEntity;
import ru.klesh.finances.api.impl.entity.PaymentAccountsEntity;
import ru.klesh.finances.api.impl.entity.PaymentsEntity;
import ru.klesh.finances.api.impl.entity.PaymentsFilterEntity;
import ru.klesh.finances.api.impl.entity.RichestEntity;
import ru.klesh.finances.api.impl.entity.ServerSessionEntity;
import ru.klesh.finances.api.impl.entity.SingInEntity;
import ru.klesh.finances.utils.MessageHelper;
import ru.klesh.finances.utils.StringFormater;
import ru.klesh.finances.utils.http.HttpClient;
import ru.klesh.finances.utils.http.HttpException;

/**
 * Created by klesh on 04.01.17.
 */

public class FinancesApi implements IFinancesApi {
    private static final FinancesApi INSTANCE = new FinancesApi();

    private AppUser appUser = null;
    private WeakReference<Context> contextWeakReference = null;

    public static FinancesApi getInstance(Context context) {
        INSTANCE.contextWeakReference = new WeakReference<>(context);
        return INSTANCE;
    }

    public static FinancesApi getInstance() {
        INSTANCE.contextWeakReference = null;
        return INSTANCE;
    }

    private void checkSession() {
        if (!isAuthorized()) {
            throw new NotAuthorizedException();
        }
    }

    public AppUser getAppUser() {
        return appUser;
    }


    public void setAppUser(AppUser appUser) {
        this.appUser = appUser;
    }

    private ServerSessionEntity.SessionData getSessionData() {
        return appUser != null ? appUser.getSessionData() : null;
    }

    @NonNull
    @Override
    public ServerSessionEntity singIn(String phone, String password) {
        SingInEntity singInEntity = new SingInEntity();
        singInEntity.setPhone(phone.startsWith("+") ? phone : "+7" + phone);
        singInEntity.setPassword(password);

        ServerSessionEntity sessionEntity;

        try {
            sessionEntity = HttpClient.post(URL_USERS_SING_IN, ServerSessionEntity.class, singInEntity);
            if (!sessionEntity.hasError()) {
                this.appUser = new AppUser(singInEntity.getPhone(), sessionEntity.getMessage(), sessionEntity.getResponseData());
            }
        } catch (HttpException e) {
            toastInternalException(e);
            sessionEntity = setErrorToEntity(e, new ServerSessionEntity());
            e.printStackTrace();
        }

        logApiResponse("Singing in", sessionEntity);
        return sessionEntity;
    }

    public boolean isAuthorized() {
        return appUser != null && appUser.getSessionData() != null;
    }


    @NonNull
    @Override
    public BalanceEntity getBalance() {
        BalanceEntity balanceEntity;

        try {
            balanceEntity = HttpClient.post(URL_USERS_BALANCE, BalanceEntity.class, getSessionData());
        } catch (HttpException e) {
            toastInternalException(e);
            balanceEntity = setErrorToEntity(e, new BalanceEntity());
            e.printStackTrace();
        }

        logApiResponse("Getting balance", balanceEntity);
        return balanceEntity;
    }

    @NonNull
    @Override
    public RichestEntity getRichest() {
        RichestEntity richestEntity;

        try {
            richestEntity = HttpClient.post(URL_USERS_RICHEST, RichestEntity.class, getSessionData());
        } catch (HttpException e) {
            toastInternalException(e);
            richestEntity = setErrorToEntity(e, new RichestEntity());
            e.printStackTrace();
        }

        logApiResponse("Getting richest", richestEntity);
        return richestEntity;
    }

    @NonNull
    @Override
    public PaymentAccountsEntity getPaymentAccounts() {
        checkSession();
        PaymentAccountsEntity paymentAccountsEntity;

        try {
            paymentAccountsEntity = HttpClient.post(URL_PAYMENT_ACCOUNTS_GET, PaymentAccountsEntity.class, getSessionData());
        } catch (HttpException e) {
            toastInternalException(e);
            paymentAccountsEntity = setErrorToEntity(e, new PaymentAccountsEntity());
            e.printStackTrace();
        }

        logApiResponse("Getting payment accounts", paymentAccountsEntity);
        return paymentAccountsEntity;
    }

    @NonNull
    @Override
    public IdEntity addPaymentAccount(PaymentAccountsEntity.PaymentAccountData accountData) {
        checkSession();
        IdEntity idEntity;

        try {
            idEntity = HttpClient.post(URL_PAYMENT_ACCOUNTS_ADD, IdEntity.class, accountData, getSessionData());
        } catch (HttpException e) {
            toastInternalException(e);
            idEntity = setErrorToEntity(e, new IdEntity());
            e.printStackTrace();
        }

        logApiResponse("Adding payment account", idEntity);
        return idEntity;
    }

    @NonNull
    @Override
    public ResponseEntity editPaymentAccount(PaymentAccountsEntity.PaymentAccountData accountData) {
        throw new RuntimeException("Not implemented");

//        checkSession();
//        ResponseEntity entity;
//
//        try {
//            entity = HttpClient.post(URL_PAYMENT_ACCOUNTS_EDIT, ResponseEntity.class, accountData, getSessionData());
//        } catch (HttpException e) {
//            toastInternalException(e);
//            entity = setErrorToEntity(e, new ResponseEntity());
//            e.printStackTrace();
//        }
//
//        logApiResponse("Editing payment account", entity);
//        return entity;
    }

    @NonNull
    @Override
    public ResponseEntity removePaymentAccount(IdEntity.IdData accountIdData) {
        throw new RuntimeException("Not implemented");

//        checkSession();
//        ResponseEntity entity;
//
//        try {
//            entity = HttpClient.post(URL_PAYMENT_ACCOUNTS_REMOVE, ResponseEntity.class, accountIdData, getSessionData());
//        } catch (HttpException e) {
//            toastInternalException(e);
//            entity = setErrorToEntity(e, new ResponseEntity());
//            e.printStackTrace();
//        }
//
//        logApiResponse("Removing payment account", entity);
//        return entity;
    }

    @NonNull
    @Override
    public PaymentsEntity getPayments() {
        return getPayments(null);
    }

    @NonNull
    @Override
    public PaymentsEntity getPayments(PaymentsFilterEntity filterEntity) {
        checkSession();
        PaymentsEntity paymentsEntity;

        try {
            paymentsEntity = HttpClient.post(URL_PAYMENTS_GET, PaymentsEntity.class, getSessionData(), filterEntity);
        } catch (HttpException e) {
            toastInternalException(e);
            paymentsEntity = setErrorToEntity(e, new PaymentsEntity());
            e.printStackTrace();
        }

        logApiResponse("Getting payments", paymentsEntity);
        return paymentsEntity;
    }

    @NonNull
    @Override
    public IdEntity addPayment(PaymentsEntity.PaymentData paymentData) {
        checkSession();
        IdEntity idEntity;

        try {
            idEntity = HttpClient.post(URL_PAYMENTS_ADD, IdEntity.class, paymentData, getSessionData());
        } catch (HttpException e) {
            toastInternalException(e);
            idEntity = setErrorToEntity(e, new IdEntity());
            e.printStackTrace();
        }

        logApiResponse("Adding payment", idEntity);
        return idEntity;
    }

    @NonNull
    @Override
    public ResponseEntity editPayment(PaymentsEntity.PaymentData paymentData) {
        checkSession();
        ResponseEntity entity;

        try {
            entity = HttpClient.post(URL_PAYMENTS_EDIT, ResponseEntity.class, paymentData, getSessionData());
        } catch (HttpException e) {
            toastInternalException(e);
            entity = setErrorToEntity(e, new ResponseEntity());
            e.printStackTrace();
        }

        logApiResponse("Editing payment", entity);
        return entity;
    }

    @NonNull
    @Override
    public ResponseEntity removePayment(IdEntity.IdData paymentIdData) {
        checkSession();
        ResponseEntity entity;

        try {
            entity = HttpClient.post(URL_PAYMENTS_REMOVE, ResponseEntity.class, paymentIdData, getSessionData());
        } catch (HttpException e) {
            toastInternalException(e);
            entity = setErrorToEntity(e, new ResponseEntity());
            e.printStackTrace();
        }

        logApiResponse("Removing payment", entity);
        return entity;
    }

    @NonNull
    @Override
    public CategoriesEntity getCategories() {
        checkSession();
        CategoriesEntity categoriesEntity;

        try {
            categoriesEntity = HttpClient.post(URL_CATEGORIES_GET, CategoriesEntity.class, getSessionData());
        } catch (HttpException e) {
            toastInternalException(e);
            categoriesEntity = setErrorToEntity(e, new CategoriesEntity());
            e.printStackTrace();
        }

        logApiResponse("Getting categories", categoriesEntity);
        return categoriesEntity;
    }

    @NonNull
    @Override
    public IdEntity addCategory(CategoriesEntity.CategoryData categoryData) {
        checkSession();
        IdEntity idEntity;

        try {
            idEntity = HttpClient.post(URL_CATEGORIES_ADD, IdEntity.class, categoryData, getSessionData());
        } catch (HttpException e) {
            toastInternalException(e);
            idEntity = setErrorToEntity(e, new IdEntity());
            e.printStackTrace();
        }

        logApiResponse("Adding category", idEntity);
        return idEntity;
    }

    @NonNull
    @Override
    public ResponseEntity editCategory(CategoriesEntity.CategoryData categoryData) {
        throw new RuntimeException("Not implemented");

//        checkSession();
//        CategoriesEntity categoriesEntity;
//
//        try {
//            categoriesEntity = HttpClient.post(URL_CATEGORIES_EDIT, CategoriesEntity.class, categoryData, getSessionData());
//        } catch (HttpException e) {
//            toastInternalException(e);
//            categoriesEntity = setErrorToEntity(e, new CategoriesEntity());
//            e.printStackTrace();
//        }
//
//        logApiResponse("Editing category", categoriesEntity);
//        return categoriesEntity;
    }

    @NonNull
    @Override
    public ResponseEntity removeCategory(IdEntity.IdData categoryIdData) {
        throw new RuntimeException("Not implemented");

//        checkSession();
//        ResponseEntity entity;
//
//        try {
//            entity = HttpClient.post(URL_CATEGORIES_REMOVE, ResponseEntity.class, categoryIdData, getSessionData());
//        } catch (HttpException e) {
//            toastInternalException(e);
//            entity = setErrorToEntity(e, new ResponseEntity());
//            e.printStackTrace();
//        }
//
//        logApiResponse("Removing category", entity);
//        return entity;
    }

    @NonNull
    @Override
    public ResponseEntity getIncome() {
        throw new RuntimeException("Not implemented!");
    }

    @NonNull
    @Override
    public ResponseEntity getOutcome() {
        throw new RuntimeException("Not implemented!");
    }

    private <T extends ResponseEntity> T setErrorToEntity(Throwable t, T entity) {
        entity.setError("Error: " + t.getMessage());
        entity.setMessage("\nStackTrace:\n at " + StringFormater.join("\n at ", (Object[]) t.getStackTrace()));
        return entity;
    }

    private void toastInternalException(HttpException e) {
        if (contextWeakReference == null) {
            return;
        }

        Context context = contextWeakReference.get();

        if (context == null) {
            return;
        }

        if (Looper.myLooper() == null) {
            Looper.prepare();
        }

        MessageHelper.toastMessage(context, R.string.template_internal_error, e.getMessage());
    }

    public static void logApiResponse(String action, ResponseEntity responseEntity) {
        if (responseEntity.hasError()) {
            Log.e(TAG_API_ERROR, "Action: " + action + ". Error: " + responseEntity.getError());
        }

        if (responseEntity.hasMessage()) {
            Log.i(TAG_API_MESSAGE, "Action: " + action + ". Message: " + responseEntity.getMessage());
        }
    }
}
