package ru.klesh.finances.api.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by klesh on 04.01.17.
 */

public class ResponseEntity<V> implements Entity {

    private String error;
    private String message;

    @SerializedName("response_data")
    private V responseData;

    public V getResponseData() {
        return responseData;
    }

    public void setResponseData(V responseData) {
        this.responseData = responseData;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean hasError() {
        return error != null && error.length() > 0;
    }

    public boolean hasMessage() {
        return message != null && message.length() > 0;
    }
}
