package ru.klesh.finances.api;

import android.support.annotation.NonNull;

import ru.klesh.finances.api.entity.IdEntity;
import ru.klesh.finances.api.entity.ResponseEntity;
import ru.klesh.finances.api.impl.AppUser;
import ru.klesh.finances.api.impl.entity.BalanceEntity;
import ru.klesh.finances.api.impl.entity.CategoriesEntity;
import ru.klesh.finances.api.impl.entity.PaymentAccountsEntity;
import ru.klesh.finances.api.impl.entity.PaymentsEntity;
import ru.klesh.finances.api.impl.entity.PaymentsFilterEntity;
import ru.klesh.finances.api.impl.entity.RichestEntity;
import ru.klesh.finances.api.impl.entity.ServerSessionEntity;

/**
 * Created by klesh on 04.01.17.
 */

public interface IFinancesApi {

    String SITE_AUTHOR_VK = "https://vk.com/im?sel=92682082";

    String URL_SITE = "https://finances.savelev.xyz";

    String URL_USERS_SING_IN = URL_SITE + "/json/users/signin";
    String URL_USERS_BALANCE = URL_SITE + "/json/users/balance";
    String URL_USERS_RICHEST = URL_SITE + "/json/users/richest";

    String URL_PAYMENTS_GET = URL_SITE + "/json/payments/get";
    String URL_PAYMENTS_ADD = URL_SITE + "/json/payments/add";
    String URL_PAYMENTS_EDIT = URL_SITE + "/json/payments/edit";
    String URL_PAYMENTS_REMOVE = URL_SITE + "/json/payments/remove";

    String URL_CATEGORIES_GET = URL_SITE + "/json/categories/get";
    String URL_CATEGORIES_ADD = URL_SITE + "/json/categories/add";
    String URL_CATEGORIES_EDIT = URL_SITE + "/json/categories/edit";
    String URL_CATEGORIES_REMOVE = URL_SITE + "/json/categories/remove";

    String URL_CATEGORIES_GET_INCOME = URL_SITE + "/json/categories/get_income";
    String URL_CATEGORIES_GET_OUTCOME = URL_SITE + "/json/categories/get_outcome";

    String URL_PAYMENT_ACCOUNTS_GET = URL_SITE + "/json/accounts/get";
    String URL_PAYMENT_ACCOUNTS_ADD = URL_SITE + "/json/accounts/add";
    String URL_PAYMENT_ACCOUNTS_EDIT = URL_SITE + "/json/accounts/edit";
    String URL_PAYMENT_ACCOUNTS_REMOVE = URL_SITE + "/json/accounts/remove";

    String TAG_API_ERROR = "FinancesAPI.Error";
    String TAG_API_MESSAGE = "FinancesAPI.Message";

    boolean isAuthorized();

    AppUser getAppUser();

    void setAppUser(AppUser appUser);

    @NonNull
    ServerSessionEntity singIn(String phone, String password);

    @NonNull
    BalanceEntity getBalance();

    @NonNull
    RichestEntity getRichest();

    @NonNull
    PaymentAccountsEntity getPaymentAccounts();

    @NonNull
    IdEntity addPaymentAccount(PaymentAccountsEntity.PaymentAccountData accountData);

    @NonNull
    ResponseEntity editPaymentAccount(PaymentAccountsEntity.PaymentAccountData accountData);

    @NonNull
    ResponseEntity removePaymentAccount(IdEntity.IdData accountIdData);

    @NonNull
    PaymentsEntity getPayments();

    @NonNull
    PaymentsEntity getPayments(PaymentsFilterEntity filterData);

    @NonNull
    IdEntity addPayment(PaymentsEntity.PaymentData paymentData);

    @NonNull
    ResponseEntity editPayment(PaymentsEntity.PaymentData paymentData);

    @NonNull
    ResponseEntity removePayment(IdEntity.IdData paymentIdData);

    @NonNull
    CategoriesEntity getCategories();

    @NonNull
    IdEntity addCategory(CategoriesEntity.CategoryData categoryData);

    @NonNull
    ResponseEntity editCategory(CategoriesEntity.CategoryData categoryData);

    @NonNull
    ResponseEntity removeCategory(IdEntity.IdData categoryIdData);

    @NonNull
    ResponseEntity getIncome();

    @NonNull
    ResponseEntity getOutcome();
}
