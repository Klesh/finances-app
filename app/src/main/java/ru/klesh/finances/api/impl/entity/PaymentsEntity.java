package ru.klesh.finances.api.impl.entity;

import com.google.gson.annotations.SerializedName;

import java.util.Comparator;

import ru.klesh.finances.api.entity.IAggregableEntity;
import ru.klesh.finances.api.entity.IFilterableEntity;
import ru.klesh.finances.api.entity.RepositoryEntity;
import ru.klesh.finances.api.entity.ResponseEntity;

/**
 * Created by klesh on 05.01.17.
 */

public class PaymentsEntity extends ResponseEntity<PaymentsEntity.PaymentData[]> {

    public static class PaymentData extends RepositoryEntity<Integer> implements IFilterableEntity, IAggregableEntity<PaymentData> {

        public static final transient Comparator<PaymentData> PAYMENT_DATA_COMPARATOR = new Comparator<PaymentData>() {
            @Override
            public int compare(PaymentData t0, PaymentData t1) {
                return (int) (t1.getTime() - t0.getTime());
            }
        };

        public static final String AGGREGATOR_TYPE_DATE = "date";

        private Integer value;
        private Long time;
        private String comment;
        private String category;

        @SerializedName("account_id")
        private Integer accountId;

        @SerializedName("category_id")
        private Integer categoryId;

        public Integer getValue() {
            return value;
        }

        public void setValue(Integer value) {
            this.value = value;
        }

        public Long getTime() {
            return time;
        }

        public void setTime(Long time) {
            this.time = time;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public Integer getAccountId() {
            return accountId;
        }

        public void setAccountId(Integer accountId) {
            this.accountId = accountId;
        }

        public Integer getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(Integer categoryId) {
            this.categoryId = categoryId;
        }

        @Override
        public String toString() {
            return String.format(
                    "PaymentsEntity: { " +
                            "id = %s, " +
                            "categoryId = %s, " +
                            "category = %s, " +
                            "value = %s, " +
                            "comment = %s, " +
                            "time = %s}",
                    id,
                    categoryId,
                    category,
                    value,
                    comment,
                    time
            );
        }

        @Override
        public boolean canSearch(String search) {
            if (getComment().toLowerCase().contains(search)) {
                return true;
            }

            if (getCategory().toLowerCase().contains(search)) {
                return true;
            }

            if (String.valueOf(getValue()).contains(search)) {
                return true;
            }

            return false;
        }

        @Override
        public boolean canAggregate(String type, PaymentData entity) {
            switch (type) {
                case AGGREGATOR_TYPE_DATE:
                    return Math.abs(entity.getTime() - getTime()) <= 3600;
            }
            return false;
        }
    }


}
