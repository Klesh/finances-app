package ru.klesh.finances.api.impl.entity;

import com.google.gson.annotations.SerializedName;

import ru.klesh.finances.api.entity.Entity;
import ru.klesh.finances.api.entity.ResponseEntity;

/**
 * Created by klesh on 04.01.17.
 */

public class ServerSessionEntity extends ResponseEntity<ServerSessionEntity.SessionData> {

    public static class SessionData implements Entity {

        @SerializedName("user_id")
        private String userId;

        @SerializedName("session_id")
        private String sessionId;

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getSessionId() {
            return sessionId;
        }

        public void setSessionId(String sessionId) {
            this.sessionId = sessionId;
        }

        @Override
        public String toString() {
            return String.format("SessionData: {userId = %s, sessionId = %s}", userId, sessionId);
        }
    }
}
