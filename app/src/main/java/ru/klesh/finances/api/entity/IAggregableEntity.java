package ru.klesh.finances.api.entity;

/**
 * Created by Klesh on 23.01.2017.
 */

public interface IAggregableEntity<V extends Entity> {
    boolean canAggregate(String type, V entity);
}
