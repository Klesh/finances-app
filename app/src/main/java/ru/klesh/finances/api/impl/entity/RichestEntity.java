package ru.klesh.finances.api.impl.entity;

import com.google.gson.annotations.SerializedName;

import ru.klesh.finances.api.entity.Entity;
import ru.klesh.finances.api.entity.ResponseEntity;

/**
 * Created by klesh on 05.01.17.
 */

public class RichestEntity extends ResponseEntity<RichestEntity.RichestData[]> {

    public static class RichestData implements Entity {

        private Long money;

        @SerializedName("first_name")
        private String firstName;

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public Long getMoney() {
            return money;
        }

        public void setMoney(Long money) {
            this.money = money;
        }

        @Override
        public String toString() {
            return String.format("RichestData: {money = %s, firstName = %s}", money, firstName);
        }
    }
}
