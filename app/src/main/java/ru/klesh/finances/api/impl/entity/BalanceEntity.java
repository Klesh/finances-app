package ru.klesh.finances.api.impl.entity;

import ru.klesh.finances.api.entity.Entity;
import ru.klesh.finances.api.entity.ResponseEntity;

/**
 * Created by klesh on 05.01.17.
 */

public class BalanceEntity extends ResponseEntity<BalanceEntity.BalanceData> {

    public static class BalanceData implements Entity {

        private Long balance;

        public Long getBalance() {
            return balance;
        }

        public void setBalance(Long balance) {
            this.balance = balance;
        }

        @Override
        public String toString() {
            return String.format("BalanceData: {balance = %s}", balance);
        }
    }
}
