package ru.klesh.finances.api.impl;

import ru.klesh.finances.api.impl.entity.ServerSessionEntity;

/**
 * Created by klesh on 11.01.17.
 */

public class AppUser {

    private String phone;
    private String name;
    private ServerSessionEntity.SessionData sessionData;

    public AppUser(String phone, String name, ServerSessionEntity.SessionData sessionData) {
        this.phone = phone;
        this.name = name;
        this.sessionData = sessionData;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ServerSessionEntity.SessionData getSessionData() {
        return sessionData;
    }

    public void setSessionData(ServerSessionEntity.SessionData sessionData) {
        this.sessionData = sessionData;
    }
}
