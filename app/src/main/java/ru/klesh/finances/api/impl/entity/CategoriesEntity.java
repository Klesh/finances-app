package ru.klesh.finances.api.impl.entity;

import com.google.gson.annotations.SerializedName;

import ru.klesh.finances.api.entity.IFilterableEntity;
import ru.klesh.finances.api.entity.RepositoryEntity;
import ru.klesh.finances.api.entity.ResponseEntity;

/**
 * Created by klesh on 05.01.17.
 */

public class CategoriesEntity extends ResponseEntity<CategoriesEntity.CategoryData[]> {

    public static class CategoryData extends RepositoryEntity<Integer> implements IFilterableEntity {

        @SerializedName("user_id")
        private Long userId;

        private String name;

        private transient Long totalMoney = 0L;

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Long getTotalMoney() {
            return totalMoney;
        }

        public void setTotalMoney(Long totalMoney) {
            this.totalMoney = totalMoney;
        }

        @Override
        public String toString() {
            return String.format("CategoryData: {" +
                            "id = %s, " +
                            "userId = %s, " +
                            "name = %s, " +
                            "totalMoney = %s" +
                            "}",
                    id, userId, name, totalMoney);
        }

        @Override
        public boolean canSearch(String search) {
            return getName().toLowerCase().contains(search);
        }
    }
}
