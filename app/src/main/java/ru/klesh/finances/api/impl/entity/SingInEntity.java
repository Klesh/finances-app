package ru.klesh.finances.api.impl.entity;

import ru.klesh.finances.api.entity.Entity;

/**
 * Created by klesh on 04.01.17.
 */

public class SingInEntity implements Entity {

    private String phone;
    private String password;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return String.format("SingInEntity: {phone = %s, password = %s}", phone, password);
    }
}
